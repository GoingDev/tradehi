
var ref = firebase.database().ref('Notic');
ref.on("value", function(snap) {
    const userid = User.id;
    let obj = [];
    let notic = snap.val();
    for(let i in notic){
        if(notic[i].notic_seen == userid){
            let data = {
                action : notic[i].notic_action,
                created : notic[i].notic_created,
                link : notic[i].notic_link,
                status : notic[i].notic_status,
                key : i,
                u_id : notic[i].notic_uid,
                name : notic[i].notic_name,
                seen : notic[i].notic_seen,
                type : notic[i].notic_type
            }
            obj.push(data);
        }
    }
    nav.noticData = _.orderBy(obj,['created'],['desc']);
});

const nav = new Vue({
    el : '#nav',
    data : {
        noticData : [],
        notic_count : 0,
        notic_show : false,
        show_count : false,
        notic_show_mobile: false,
    },
    watch : {
        noticData(val){
            this.notic_count = 0;
            for(let i in this.noticData){
                if(this.noticData[i].status == 0){
                    this.notic_count++;
                }
            }
            this.notic_count != 0 ? this.show_count = true  : this.show_count = false
            if(User.noticData != undefined) User.noticData = val ;
        }
    },
    methods:{
        close(){
            this.notic_show  = false;
        },
        noticBtnClick(){
            this.notic_show = !this.notic_show
            let data = this.noticData;
            for(let i in data){
                if(data[i].status == 0){
                    let updates = {
                        notic_action: data[i].action,
                        notic_created:  data[i].created,
                        notic_link: data[i].link,
                        notic_name: data[i].name,
                        notic_seen:  data[i].seen,
                        notic_status:  1,
                        notic_type: data[i].type,
                        notic_uid: data[i].u_id
                    }
                    firebase.database().ref('Notic/'+data[i].key).update(updates);
                }
            }
        },
        noticBtnMobileClick() {
            this.notic_show_mobile = !this.notic_show_mobile;
        },
        noticBtnMobileClickClose(){
            this.notic_show_mobile = false;
        },
        async gotoLink(link,data){
                let updates = await {
                    notic_action: data.action,
                    notic_created:  data.created,
                    notic_link: data.link,
                    notic_name: data.name,
                    notic_seen:  data.seen,
                    notic_status:  2,
                    notic_type: data.type,
                    notic_uid: data.u_id
                }
            let firebaseupdate = await firebase.database().ref('Notic/'+data.key).update(updates);
            window.location.assign(link);
        }
    },
    directives: {
        'click-outside': {
            bind: function(el, binding, vNode) {
            // Provided expression must evaluate to a function.
            if (typeof binding.value !== 'function') {
                const compName = vNode.context.name
              let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
              if (compName) { warn += `Found in component '${compName}'` }

              console.warn(warn)
            }
            // Define Handler and cache it on the element
            const bubble = binding.modifiers.bubble
            const handler = (e) => {
              if (bubble || (!el.contains(e.target) && el !== e.target)) {
                  binding.value(e)
              }
            }
            el.__vueClickOutside__ = handler

            // add Event Listeners
            document.addEventListener('click', handler)
            },
            unbind: function(el, binding) {
            // Remove Event Listeners
            document.removeEventListener('click', el.__vueClickOutside__)
            el.__vueClickOutside__ = null
            }
        }
    }
});
