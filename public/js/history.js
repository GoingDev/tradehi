var ref = firebase.database().ref('Finance');
ref.on("value", function(snap) {
    const id = User.id;
    let Deposit = snap.val().Deposit;
    let WithDraw = snap.val().WithDraw;
    let obj = [];
    for(let i in Deposit){
        if(Deposit[i].u_id == id){
            let o = {
                amount : Deposit[i].depo_fund,
                created :Deposit[i].depo_created,
                type : 0,
                status : Deposit[i].depo_status
            }
            obj.push(o);
        }
    };
    for(let j in WithDraw){
        if(WithDraw[j].u_id == id){
            let o = {
                amount : WithDraw[j].wd_amount,
                created :WithDraw[j].wd_created,
                type : WithDraw[j].wd_type,
                status : WithDraw[j].wd_status
            }
            obj.push(o);
        }
    };
    if(obj.length > 0) history.dataDip = _.orderBy(obj,['created'],['desc']);
    //type == 0 คือ ฝาก
    //type == 1 คือ แปลงเป็นทุน
    //type == 2 คือ ถอนผลตอบแทน
    //type == 3 คือ ถอนทุน
    //status == 1 คือ Wating
    //status == 2 คือ Approved
    //status == 3 คือ Rejected
});
const history = new Vue({
    el : '#history',
    data : {
        dataDip : [],
        historyLoader : true
    },
    created(){

    },
    methods: {
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
              decimalCount = Math.abs(decimalCount);
              decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

              const negativeSign = amount < 0 ? "-" : "";

              let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
              let j = (i.length > 3) ? i.length % 3 : 0;

              return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
              console.log(e)
            }
        }
    },
    watch: {
        dataDip: function(val) {
            const vm = this;
            this.historyLoader = false;
            if(val.length == 0){
                vm.dataDip = [];
            }
        }
    },
    mounted() {
        const vm = this;
        this.$nextTick(function () {
            if(vm.dataDip.length == 0 ) vm.historyLoader = false
        })
    }

});
