//firebase
var ref = firebase.database().ref('Finance/');
ref.on("value", function (snap) {
    UserDashboard.fetchAll();
    UserDashboard.getProfit();
    const id = User.id;
    let Deposit = snap.val().Deposit;
    let WithDraw = snap.val().WithDraw;
    let obj = [];
    for (let i in Deposit) {
        if (Deposit[i].u_id == id) {
            let o = {
                amount: Deposit[i].depo_fund,
                created: Deposit[i].depo_created,
                type: 0,
                status: Deposit[i].depo_status
            }
            obj.push(o);
        }
    };
    for (let j in WithDraw) {
        if (WithDraw[j].u_id == id) {
            let o = {
                amount: WithDraw[j].wd_amount,
                created: WithDraw[j].wd_created,
                type: WithDraw[j].wd_type,
                status: WithDraw[j].wd_status
            }
            obj.push(o);
        }
    };
    if (obj.length > 0) {
        UserDashboard.dataDip = _.orderBy(obj, ['created'], ['desc'])
        UserDashboard.loadPage = false;
    } else {
        UserDashboard.CanRequest = true;
        UserDashboard.loadPage = false;
    }
});
ref.on("child_added", function (data) {
    UserDashboard.fetchAll();
    UserDashboard.getProfit();
});

// start app
Vue.component('modal', {
    template: '#modal-template',
    props: ['color']
})
Vue.component('content-loader', {
    template: '#content-loader',
})
const UserDashboard = new Vue({
    el: '#app',
    data: {
        profit: 0,
        fund: 0,
        allfund: 0,
        allreturn: 0,
        allData: [],
        allprofit: [],
        allcom: 0,
        invester: [],
        dataDip: [],
        dataDipLoader: true,
        allprofitLoader: true,
        investerLoader: true,
        allreturnLoader: true,
        stag: 0,
        CanRequest: false,
        loadPage: true
    },
    created() {
        this.fetchAll();
        this.getProfit();
    },
    computed: {
        total() {
            return this.allreturn + this.fund + this.stag;
        },
        grow() {
            return ((this.allreturn / this.fund) * 100).toFixed(2);
        },
        moneyFundList() {
            let result = this.dataDip.filter((q) => q.type == 0 && q.status == 2 || q.type == 3 && q.status == 2)
            return result;
        },
        moneyFundTotal() {
            let result = this.moneyFundList.reduce((sum,q)=> {
                if(q.type == 0){
                    return sum + parseInt(q.amount)
                } else if(q.type == 3){
                   return sum - parseInt(q.amount)
                }
            },0)
            return result;
        }
    },
    methods: {
        DepositPanel() {
            if (this.dataDipLoader == false) {
                appmodalDeposit.showModal = true
                navFooter.showNav = true
            } else {
                return false;
            }

        },
        WithDrawPanel() {
            if (this.dataDipLoader == false) {
                if (this.CanRequest == true) {
                    appmodalWithDraw.showModal = true
                    navFooter.showNav = true
                } else {
                    alertFail('Please waiting for another request done,Then you will get a new one.');
                }
            } else {
                return false;
            }
        },
        fetchAll() {
            const vm = this;
            axios.get('/api/getAllmember').then((res) => {
                return vm.allData = res.data;
            });
        },
        getProfit() {
            const vm = this;
            let id = User.id;
            axios.get('/api/getProfit/' + id).then((res) => {
                return vm.allprofit = _.orderBy(res.data, ['created_at'], ['desc']);
            });
        },
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
                console.log(e)
            }
        },
        CalRation(fund) {
            return (fund / this.allfund) * 100;
        }
    },
    watch: {
        allData(val) {
            this.fund = parseFloat(val.find((q) => q.id == User.id).member_fund)
            this.profit = parseFloat(val.find((q) => q.id == User.id).member_profit) + parseFloat(val.find((q) => q.id == User.id).member_balance)
            this.allreturn = parseFloat(val.find((q) => q.id == User.id).member_profit)
            this.allcom = parseFloat(val.find((q) => q.id == User.id).member_commission)
            this.allfund = val.reduce((sum, number) => sum + parseFloat(number.member_fund), 0)
            this.invester = _.orderBy(val, ['paeseFloat(member_fund)'], ['desc']).slice(0, 10)
            this.stag = parseFloat(val.find((q) => q.id == User.id).member_stag)
        },
        dataDip(val) {
            this.dataDipLoader = false
            let CheckRequest = val.filter((q) => q.type != 0 && q.status == 1);
            if (val.length > 0) {
                if (CheckRequest.length > 0) {
                    this.CanRequest = false;
                } else if (CheckRequest.length == 0) {
                    this.CanRequest = true;
                }
            } else if (val.length == 0) {
                this.CanRequest = true;
            }
        },
        allprofit(val) {
            this.allprofitLoader = false
        },
        invester(val) {
            this.investerLoader = false
        },
        allreturn(val) {
            this.allreturnLoader = false
        },
        allfund(val) {
            this.allreturnLoader = false
        }
    },
    mounted() {
        const vm = this;
        this.$nextTick(function () {
            TweenMax.staggerFrom($('.fade-icon'), .8, {
                opacity: 0,
                x: 20
            }, .25)
            if (vm.dataDip.length == 0) vm.dataDipLoader = false
        })
    }
})

const appmodalDeposit = new Vue({
    el: '#appmodalDeposit',
    data: {
        fund: '',
        showModal: false,
        Waiting: false,
        Slip: ' '
    },
    methods: {
        onFileChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            this.Slip = e.target.files[0];
        },
        AddFund(e) {
            const vm = this;
            if (vm.Slip == ' ' || vm.fund == 0) {
                swal({
                    title: 'Failed !',
                    text: 'Please enter your data',
                    type: 'error'
                })
            } else {
                vm.Waiting = true;
                let id = User.id;
                let fd = new FormData();
                const config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                }
                fd.append('file', vm.Slip);
                fd.append('fund', vm.fund);
                fd.append('id', id);
                axios.post('/api/AddFund', fd, config)
                    .then((res) => {
                        swal({
                            title: 'Success !',
                            text: 'waiting for verify',
                            type: 'success'
                        })
                    })
                    .catch((err) => {
                        alertFail('Please try again')
                    })
                    .then((res) => {
                        vm.CloseModal();
                    });
            }
        },
        CloseModal(e) {
            const vm = this;
            vm.showModal = false;
            navFooter.showNav = false;
            vm.Waiting = false;
            vm.fund = '';
            vm.Slip = '';
        }
    }
});

const appmodalWithDraw = new Vue({
    el: '#appmodalWithDraw',
    data: {
        type: '1',
        withdraw: '',
        showModal: false,
        Waiting: false,
    },
    methods: {
        WDconfirm() {
            const vm = this;
            if (vm.withdraw <= 0) {
                alertFail('Data incorrect');
                return false;
            } else {
                if (vm.type == 1 && parseFloat(vm.withdraw) > parseFloat(UserDashboard.allreturn)) {
                    alertFail('Your Return is not enough to withdraw.');
                    return false;
                } else if (vm.type == 2 && parseFloat(vm.withdraw) > parseFloat(UserDashboard.allreturn)) {
                    alertFail('Your Return is not enough to withdraw.');
                    return false;
                } else if (vm.type == 3 && parseFloat(vm.withdraw) > parseFloat(UserDashboard.fund)) {
                    alertFail('Your Fund is not enough to withdraw.');
                    return false;
                } else if (vm.type == 4 && parseFloat(vm.withdraw) > parseFloat(UserDashboard.allcom)) {
                    alertFail('Your commission is not enough to withdraw.');
                    return false;
                } else if (vm.type == 2 && parseFloat(vm.withdraw) < ((parseFloat(UserDashboard.fund) / 100) * 5)) {
                    alertFail('Your can withdraw profit as 5% from fund.');
                    return false;
                } else if (vm.type == 4 && parseFloat(vm.withdraw) < 500) {
                    alertFail('Your can withdraw lowest commission as 500 THB');
                    return false;
                }
                vm.Waiting = true;
                let id = User.id;
                let obj = {
                    id: id,
                    amount: vm.withdraw,
                    type: vm.type
                }
                axios.post('/api/WithDraw', obj)
                    .then((res) => {
                        vm.showModal = false;
                        swal({
                            title: 'Success !',
                            text: 'waiting for verify',
                            type: 'success'
                        })
                    })
                    .catch((err) => {
                        alertFail('Please try again')
                    })
                    .then((res) => {
                        vm.CloseModal();
                    });
            }
        },
        CloseModal(e) {
            const vm = this;
            vm.showModal = false;
            navFooter.showNav = false;
            vm.Waiting = false;
            vm.type = 1;
            vm.withdraw = '';
        }
    }
});

function alertFail(massage) {
    swal({
        title: 'Failed !',
        text: massage,
        type: 'error'
    })
}



const navFooter = new Vue({
    el: '#navFooter',
    data: {
        showNav: false,
        fund: 0,
        allreturn: 0,
        allcom: 0,
        total: 0
    },
    methods: {
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
                console.log(e)
            }
        },
    }
});
UserDashboard.$watch('total', function (newValue, oldValue) {
    navFooter.total = newValue;
});
UserDashboard.$watch('fund', function (newValue, oldValue) {
    navFooter.fund = newValue;
});
UserDashboard.$watch('allreturn', function (newValue, oldValue) {
    navFooter.allreturn = newValue;
});
UserDashboard.$watch('allcom', function (newValue, oldValue) {
    navFooter.allcom = newValue;
});
