const App = new Vue({
    el : '#app',
    data : {
        file : '',
        img : '',
        isChangePassword : false,
        password : {
            oldPassword : '',
            newPassword : '',
            confirmPassword : ''
        }
    },
    methods : {
        onFileChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;
            this.createImage(files[0]);
            this.file = e.target.files[0];
        },
        createImage(file) {
            var vm = this;
            vm.img = new Image();
            var reader = new FileReader();
            reader.onload = (e) => {
                vm.img = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        changePassword(){
            var vm = this;
            axios.put('/api/password/'+ User.id, vm.password).then(function (res) {
                if(res.data == 'success'){
                swal({
                        title : 'Success!',
                        text : 'You have a new password.',
                        type : 'success'
                      });
                vm.changePasswordClear();
            }
                else swal({
                    title : 'Fail!',
                    text : 'Please try agian.',
                    type : 'error'
                  });
            });
        },
        changePasswordClear(){
            var vm = this;
            vm.password.oldPassword = '';
            vm.password.newPassword = '';
            vm.password.confirmPassword = '';
            vm.isChangePassword = false;
        }
    }
})
