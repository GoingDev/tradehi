var ref = firebase.database().ref('Finance/');
ref.on("value", function(snap) {
    invest.FetchProfit();
});
const invest = new Vue({
    el : '#profit',
    data : {
        allprofit : [],
        allprofitLoader: true

    },
    created(){
        this.FetchProfit();
    },
    methods : {
        FetchProfit(){
            const vm = this;
            let id = User.id;
            axios.get('/api/getProfit/'+id).then((res)=>{
                return vm.allprofit = _.orderBy(res.data,['created_at'],['desc']);
            });
        },
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
              decimalCount = Math.abs(decimalCount);
              decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

              const negativeSign = amount < 0 ? "-" : "";

              let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
              let j = (i.length > 3) ? i.length % 3 : 0;

              return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
              console.log(e)
            }
        }
    },
    watch: {
        allprofit: function(val){
            this.allprofitLoader = false;
        }
    }
});
