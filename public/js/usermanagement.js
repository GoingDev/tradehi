Vue.component('modal', {
    template: '#modal-template'
})
const App = new Vue({
    el: '#app',
    data: {
        datarow: [],
        loaddata: true,
        Editshow: false,
        DataEdit: {
            name: '',
            email: '',
            status: '',
            id: '',
            bookbank_name: '',
            passport: '',
            bookbank_num: ''
        },
        loaddatamember : true,
        datamember : [],
        Editmembershow : false,
        DataEditMember : {
            img : '',
            address : '',
            tel : '',
            fund : '',
            id : '',
            u_id : '',
            name: '',
            email: '',
            bookbank_name: '',
            passport: '',
            bookbank_num: '',
        },
        file : '',
        NoneUser : true,
        datamemberLoader: true
    },
    created() {
        this.RefreshData();
    },
    methods: {
        fetchData() {
            const vm = this;
            axios.get('/api/getAlluser').then(function (res) {
                let data = res.data
                vm.datarow = [];
                for (let i in data) {
                    if (data[i].status == 2) {
                        vm.datarow.push(data[i])
                    }
                }
            }).then(function () {
                if(vm.datarow.length != 0){
                    vm.NoneUser = false
                }
                else{
                    vm.NoneUser = true
                }
                vm.loaddata = false;
            });
        },
        Deluser(index) {
            const vm = this;
            const id = vm.datarow[index].id
            swal({
                title: 'Do you want to delete ?',
                text: "This data isn't will back !",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, i will !'
            }).then((result) => {
                if (result.value) {
                    vm.datarow.splice(index, 1);
                    axios.delete('/api/Deluser/' + id).then(function (res) {
                        swal(
                            'Deleted!',
                            'This account has been deleted.',
                            'success'
                        )
                    });
                }
                this.RefreshData();
            })
        },
        Edituser(index) {
            const vm = this;
            vm.DataEdit.id = vm.datarow[index].id
            vm.DataEdit.email = vm.datarow[index].email
            vm.DataEdit.name = vm.datarow[index].name
            vm.DataEdit.status = vm.datarow[index].status
            vm.DataEdit.passport = vm.datarow[index].passport
            vm.DataEdit.bookbank_name = vm.datarow[index].bankname
            vm.DataEdit.bookbank_num = vm.datarow[index].banknum
            vm.Editshow = true
            vm.Editmembershow = false
        },
        Editclick() {
            const vm = this;
            if (checkProperties(vm.DataEdit) == true && vm.DataEdit.status == "3")
            {
                swal({
                    title: 'Do you want to Approve ?',
                    text: "This account will be The member!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, i will !'
                }).then((result) => {
                    if (result.value) {
                        axios.post('/api/Edituser', vm.DataEdit
                        ).then(function (res) {
                            swal(
                                'Approve Success!',
                                'This account has been Approved.',
                                'success'
                            )
                            vm.RefreshData();
                            vm.ClearformEdit();
                        });
                    }
                })
            }
            else{
                swal({
                    title : 'Approve fail !',
                    text : 'Please completed data.',
                    type : 'error'
                })
            }
        },
        async view_img(index, path) {
            if (path == "passport") {
                var img = await this.datarow[index].passport_img
                appmodal.img_modal = await '/passport/' + img;
                appmodal.header = await 'PASSPORT';
            } else {
                var img = await this.datarow[index].bookbank_img
                appmodal.img_modal = await '/bookbank/' + img;
                appmodal.header = await 'BOOKBANK';
            }
            appmodal.showModal = await true;
        },
        fetchDatamember(){
            const vm = this;
            axios.get('/api/getAllmember').then(function (res) {
                let result = res.data.filter(q => q.u_id != 1)
                vm.datamember = result;
            }).then(function () {
                vm.loaddatamember = false;
            });
        },
        Editmember(index){
            const vm = this;
            let data = vm.datamember[index];
            vm.DataEditMember.id = data.member_id;
            vm.DataEditMember.u_id = data.u_id;
            if(data.member_img == null){
                vm.DataEditMember.img = "/picture/avatar.jpg"
            }
            else{
                vm.DataEditMember.img = "/picture/"+data.member_img;
            }
            vm.DataEditMember.address = data.member_address;
            vm.DataEditMember.tel = data.member_tel;
            vm.DataEditMember.fund = data.member_fund;
            vm.DataEditMember.name = data.name;
            vm.DataEditMember.email = data.email;
            vm.DataEditMember.bookbank_name = data.bankname;
            vm.DataEditMember.passport = data.passport;
            vm.DataEditMember.bookbank_num = data.banknum;
            vm.Editmembershow = true ;
            vm.Editshow = false ;
        },
        formSubmit(e){
            e.preventDefault();
            const vm = this;
            swal({
                title: 'Do you want to Edit ?',
                text: "This account will be new profile !",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, i will !'
            }).then((result) => {
                if (result.value) {
                    let  fd = new FormData();
                    const config = {
                        headers: { 'content-type': 'multipart/form-data' }
                    }
                    fd.append('file',vm.file);
                    fd.append('address',vm.DataEditMember.address);
                    fd.append('tel',vm.DataEditMember.tel);
                    fd.append('fund',vm.DataEditMember.fund);
                    fd.append('id',vm.DataEditMember.id );
                    fd.append('u_id',vm.DataEditMember.u_id);
                    fd.append('name',vm.DataEditMember.name);
                    fd.append('email',vm.DataEditMember.email);
                    fd.append('bookbank_name',vm.DataEditMember.bookbank_name);
                    fd.append('passport',vm.DataEditMember.passport);
                    fd.append('bookbank_num',vm.DataEditMember.bookbank_num);
                    axios.post('/api/Editmember',fd,config)
                    .then(function (res) {
                        swal(
                            'Approve Success!',
                            'This account has been Approved.',
                            'success'
                        )
                        vm.RefreshData();
                        vm.ClearformEditmember();
                    });
                }
            })
        },
        RefreshData(){
            this.fetchData();
            this.fetchDatamember();
        },
        Delmember(index){
            const vm = this;
            const id = vm.datamember[index].member_id
            swal({
                title: 'Do you want to delete ?',
                text: "This data isn't will back !",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, i will !'
            }).then((result) => {
                if (result.value) {
                    axios.delete('/api/Delmember/' + id).then(function (res) {
                        vm.datamember.splice(index, 1);
                        swal(
                            'Deleted!',
                            'This account has been deleted.',
                            'success'
                        )
                    });
                }
            })
        },
        onFileChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;
            this.createImage(files[0]);
            this.file = e.target.files[0];
        },
        createImage(file) {
            var vm = this;
            vm.DataEditMember.img = new Image();
            var reader = new FileReader();
            reader.onload = (e) => {
                vm.DataEditMember.img = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        ClearformEdit() {
            this.Editshow = false;
            this.DataEdit.name = ''
            this.DataEdit.email = ''
            this.DataEdit.status = ''
            this.DataEdit.id = ''
            this.DataEdit.bookbank_name = '',
                this.DataEdit.passport = '',
                this.DataEdit.bookbank_num = ''
        },
        ClearformEditmember() {
            this.Editmembershow = false;
            this.DataEditMember.img  = '';
            this.DataEditMember.address  = '';
            this.DataEditMember.tel  = '';
            this.DataEditMember.fund  = '';
            this.DataEditMember.id  = '';
            this.DataEditMember.u_id  = '';
            this.DataEditMember.name = '';
            this.DataEditMember.email = '';
            this.DataEditMember.bookbank_name = '';
            this.DataEditMember.passport = '';
            this.DataEditMember.bookbank_num = '';
        },
    },
    watch: {
        datamember(val){
            this.datamemberLoader = false
        }
    }
});

const appmodal = new Vue({
    el: '#appmodal',
    data: {
        showModal: false,
        img_modal: '',
        header: ''
    }
});

function checkProperties(obj) {
    var count = 0;
    for (var key in obj) {
        if (obj[key] !== null && obj[key] != ""){

        }
        else{
            count++;
        }
    }
    if(count !=0){
        return false;
    }
    else{
        return true;
    }

}
