var ref = firebase.database().ref('Finance/');
ref.on("value", function (snap) {
    let Deposit = snap.val().Deposit;
    let WithDraw = snap.val().WithDraw;
    Admin.dataDip = _.merge(Deposit, WithDraw);
});
ref.on("child_changed", function (data) {
    Admin.FetchUser();
});
ref.on("child_added", function (data) {
    Admin.FetchUser();
});
ref.on("child_removed", function (data) {
    Admin.FetchUser();
});
Vue.component('modal', {
    template: '#modal-template'
})

const Admin = new Vue({
    el: '#viewall',
    data: {
        dataReq: [],
        dataUser: [],
        dataDip: [],
        dataApproved: [],
        dataWaiting: [],
        Loaddata: false,
        pending : false,
        filtext : '',
        wdtotal : null
    },
    created() {
        this.FetchUser();
    },
    computed : {
        filteredList() {
            return this.dataApproved.filter((post) => {
                var alltext = post.investor.toLowerCase()
              return alltext.includes(this.filtext.toLowerCase())
            })
        },
    },
    methods: {
        FetchData(data) {
            const vm = this;
            let datarow = data;
            vm.dataReq = [];
            let obj = [];
            for (let i in datarow) {
                let result = vm.dataUser.find(q => q.id == datarow[i].u_id);
                if (_.hasIn(datarow[i], 'wd_type')) {
                    let dt = datarow[i].wd_created.split(" ");
                    let type = CheckType(parseInt(datarow[i].wd_type));
                    let row = {
                        code: datarow[i].wd_codeorder,
                        date: dt[0],
                        time: dt[1],
                        amount: datarow[i].wd_amount,
                        type: type,
                        investor: result.name,
                        img: ' ',
                        status: datarow[i].wd_status,
                        u_id: datarow[i].u_id,
                        key: i,
                        bank: result.bankname + ' : ' + result.banknum,
                        updated : datarow[i].updated
                    }
                    obj.push(row)
                } else {
                    let dt = datarow[i].depo_created.split(" ");
                    let row = {
                        code: datarow[i].depo_codeorder,
                        date: dt[0],
                        time: dt[1],
                        amount: datarow[i].depo_fund,
                        type: 'Deposit',
                        investor: result.name,
                        img: datarow[i].depo_verifyimg,
                        status: datarow[i].depo_status,
                        u_id: datarow[i].u_id,
                        key: i,
                        bank: result.bankname + ' : ' + result.banknum,
                        updated : datarow[i].updated
                    }
                    obj.push(row)
                }
            }
            vm.dataReq = _.orderBy(obj, ['date', 'time'], ['desc', 'desc']);
        },
        FetchUser() {
            const vm = this;
            axios.get('/api/getAllmember').then((res) => {
                return vm.dataUser = res.data
            });
        },
        View(index) {
            appmodal.showModal = true;
            let img = this.dataWaiting[index].img;
            appmodal.img_modal = '/slip/' + img;
        },
        View2(index) {
            appmodal.showModal = true;
            let img = this.dataApproved[index].img;
            appmodal.img_modal = '/slip/' + img;
        },
        Approve(index, type) {
            swal({
                title: 'Do you want to Approve ?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, i will !'
            }).then((result) => {
                if (result.value) {
                    let updates, key;
                    if (type == "Deposit") {
                        let data = this.dataWaiting[index];
                        key = data.key;
                        updates = {
                            depo_codeorder: data.code,
                            depo_created: data.date + " " + data.time,
                            depo_fund: data.amount,
                            depo_status: 2,
                            depo_verifyimg: data.img,
                            u_id: data.u_id,
                            updated : getTime()
                        }
                    } else {
                        let data = this.dataWaiting[index];
                        key = data.key;
                        let type = CheckReverseType(this.dataWaiting[index].type);
                        updates = {
                            wd_codeorder: data.code,
                            wd_created: data.date + " " + data.time,
                            wd_amount: data.amount,
                            wd_status: 2,
                            u_id: data.u_id,
                            wd_type: type,
                            updated : getTime()
                        }
                    }
                    axios.get('/api/ApproveRequests', {
                        params: {
                            update: updates,
                            key: key
                        }
                    }).then((res) => {
                        if (res.data.success == 'done') {
                            swal({
                                title: 'Success !',
                                text: 'Approved Success',
                                type: 'success',
                            })
                        } else {
                            swal({
                                title: 'Failed !',
                                text: res.data.success,
                                type: 'error',
                            })
                        }
                    });
                }
            })
        },
        Reject(index, type) {
            swal({
                title: 'Do you want to Reject ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, i will !'
            }).then((result) => {
                if (result.value) {
                    if (type == "Deposit") {
                        let data = this.dataWaiting[index];
                        let key = data.key;
                        let updates = {
                            depo_codeorder: data.code,
                            depo_created: data.date + " " + data.time,
                            depo_fund: data.amount,
                            depo_status: 3,
                            depo_verifyimg: data.img,
                            u_id: data.u_id,
                            updated : getTime()
                        }
                        ref.child('Deposit').child(key).update(updates);
                        let updatesnotic = {
                            notic_action: "ไม่อนุมัติการฝากเงิน",
                            notic_created: getTime(),
                            notic_link: "/History",
                            notic_name: "Admin",
                            notic_seen: data.u_id,
                            notic_status: 0,
                            notic_type: 7,
                            notic_uid: 1
                        }
                        firebase.database().ref('Notic').push(updatesnotic);
                    } else {
                        let data = this.dataWaiting[index];
                        let key = data.key;
                        let type = CheckReverseType(this.dataWaiting[index].type);
                        let updates = {
                            wd_codeorder: data.code,
                            wd_created: data.date + " " + data.time,
                            wd_amount: data.amount,
                            wd_status: 3,
                            u_id: data.u_id,
                            wd_type: type,
                            updated : getTime()
                        }
                        ref.child('WithDraw').child(key).update(updates);
                        let updatesnotic = {
                            notic_action: "ไม่อนุมัติการถอนเงิน",
                            notic_created: getTime(),
                            notic_link: "/History",
                            notic_name: "Admin",
                            notic_seen: data.u_id,
                            notic_status: 0,
                            notic_type: 6,
                            notic_uid: 1
                        }
                        firebase.database().ref('Notic').push(updatesnotic);
                    }
                }
            })
        },
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
                console.log(e)
            }
        },
        async AllTransfer() {
            let alltransfer = await this.dataWaiting.filter((q) => q.type == 'Transfer');
            if(alltransfer.length < 1){
               return false;
            }
            this.pending = true;
            this.Loaddata = false;
            for (let i in alltransfer) {
                let data = alltransfer[i];
                key = data.key;
                let type = CheckReverseType(data.type);
                updates = {
                    wd_codeorder: data.code,
                    wd_created: data.date + " " + data.time,
                    wd_amount: data.amount,
                    wd_status: 2,
                    u_id: data.u_id,
                    wd_type: type
                }
                axios.get('/api/ApproveRequests', {
                    params: {
                        update: updates,
                        key: key
                    }
                }).then((res) => {
                    if(i==alltransfer.length-1){
                        swal({
                            title: 'Success !',
                            text: 'Approved Success',
                            type: 'success',
                        })
                        this.pending = false;
                        this.Loaddata = true;
                    }
                });
            }
        }
    },
    watch: {
        dataUser(val) {
            if (val.length > 0) this.FetchData(this.dataDip);
        },
        dataReq(val) {
            this.dataApproved = val.filter((q) => q.status != 1);
            this.dataWaiting = val.filter((q) => q.status == 1);
            if (val.length > 0) this.Loaddata = true;
        },
        dataWaiting(val){
            let result = val.filter((q) => q.type==="WD Fund" || q.type==="WD Profit" || q.type==="WD Com")
            this.wdtotal = result.reduce((sum,number)=>sum+parseFloat(number.amount),0);
            console.log("total",this.wdtotal)
            console.log("result",result)
        }
    },
    updated() {
        $('[data-toggle="tooltip"]').tooltip()
    }
});

const appmodal = new Vue({
    el: '#appmodal',
    data: {
        showModal: false,
        img_modal: ''
    },
    methods: {


    }
});

function CheckType(data) {
    switch (data) {
        case 1:
            return 'Transfer'
        case 2:
            return 'WD Profit'
        case 3:
            return 'WD Fund'
        case 4:
            return 'WD Com'
    }
}

function CheckReverseType(data) {
    switch (data) {
        case 'Transfer':
            return 1
        case 'WD Profit':
            return 2
        case 'WD Fund':
            return 3
        case 'WD Com':
            return 4
    }
}

function getTime() {
    let dt = moment().format('YYYY-MM-DD HH:mm:ss');
    return dt
}
//UPDATE `members` SET `member_profit`=0,`member_balance`=0 WHERE 1
