const App = new Vue({
    el: '#app',
    data: {
        dataGroupRow: [],
        groupRateDate:{
            group: '',
            rate: ''
        },
        isEdit: false,
        isEdit_id: '',
        dataGroupRowLoader: true

    },
    created() {
        this.FetchData();
    },
    methods: {
        FetchData() {
            const vm = this;
            axios.get('/api/setting').then(function (res) {
                let data = res.data;
                vm.dataNormalRow = [];
                vm.dataGroupRow = [];
                for (let i in data) {
                    vm.dataGroupRow.push(data[i]);
                }
            });
        },
        DelRate(id) {
            const vm = this;
            axios.delete('/api/setting/'+ id).then(function (res) {
                vm.groupRateDate.group = '';
                vm.groupRateDate.rate = '';
                vm.FetchData();
                swal({
                    title : 'Deleted',
                    text : 'Delete Rate success',
                    type : 'success'
                });
          });
        },
        AddGroupRate(){
            const vm = this;
            if(parseInt(vm.groupRateDate.rate) > 100) vm.groupRateDate.rate = '100';
            axios.post('/api/setting', vm.groupRateDate).then(function (res) {
                vm.groupRateDate.group = '';
                vm.groupRateDate.rate = '';
                vm.FetchData();
                swal({
                    title : 'Success',
                    text : 'Add Rate success',
                    type : 'success'
                });
            });
        },
        EditRate(id, group, rate){
            const vm = this;
            vm.groupRateDate.group = group;
            vm.groupRateDate.rate = rate;
            vm.isEdit_id = id;
            vm.isEdit = true;
        },
        UpdateGroupRate(){
            const vm = this;
            if(parseInt(vm.groupRateDate.rate) > 100) vm.groupRateDate.rate = '100';
            axios.put('/api/setting/' +vm.isEdit_id, vm.groupRateDate).then(function (res) {
                vm.groupRateDate.group = '';
                vm.groupRateDate.rate = '';
                vm.isEdit = false;
                vm.FetchData();
                swal({
                    title : 'Success',
                    text : 'Update Rate success',
                    type : 'success'
                });
            });
        },
        Clear(){
            const vm = this;
            vm.groupRateDate.group = '';
            vm.groupRateDate.rate = '';
            vm.isEdit = false;
        }
    },
    watch: {
        dataGroupRow(val){
            this.dataGroupRowLoader = false
        }
    }
});
