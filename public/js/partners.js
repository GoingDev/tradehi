//firebase
var ref = firebase.database().ref('Finance/');
ref.on("value", function(snap) {
    partners.fetchMember();
});
ref.on("child_added", function(data) {
    partners.fetchMember();
});

const partners = new Vue({
    el : '#partners',
    data : {
        dataMember : [],
        myData : [],
        row : [],
        partnersLoader : true
    },
    created(){
        this.fetchMember();
    },
    methods :{
        fetchMember(){
            const vm = this;
            axios.get('/api/getAllmember').then((res)=>{
                return vm.dataMember = res.data;
            });
        },
        ShowChild(id){
            for(let i in this.row){
                if(this.row[i].childof == id){
                    this.row[i].show = !this.row[i].show;
                }
            }
        },
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
              decimalCount = Math.abs(decimalCount);
              decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

              const negativeSign = amount < 0 ? "-" : "";

              let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
              let j = (i.length > 3) ? i.length % 3 : 0;

              return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
              console.log(e)
            }
        }
    },
    watch : {
        dataMember(val){
            this.myData = val.find((q)=>q.id==User.id)
            this.partnersLoader = false
         },
        myData(val){
            let q = 1;
            this.row = [];
            for(let i in this.dataMember){
                if(val.id == this.dataMember[i].invite_id){
                    let obj = {
                        q : q,
                        id : this.dataMember[i].id,
                        code : this.dataMember[i].member_code,
                        name : this.dataMember[i].name,
                        profit : this.dataMember[i].member_profit,
                        com : this.dataMember[i].member_commission,
                        img : this.dataMember[i].member_img,
                        cl : 'padd2',
                        childof : val.id,
                        show : true
                    }
                    this.row.push(obj);
                    q++;
                    for(let j in this.dataMember){
                        if(this.dataMember[i].id == this.dataMember[j].invite_id){
                            let obj = {
                                q : q,
                                id : this.dataMember[j].id,
                                code : this.dataMember[j].member_code,
                                name : this.dataMember[j].name,
                                profit : this.dataMember[j].member_profit,
                                com : this.dataMember[j].member_commission,
                                img : this.dataMember[j].member_img,
                                cl : 'padd4',
                                childof : this.dataMember[i].id,
                                show : false
                            }
                            this.row.push(obj);
                            q++;
                        }
                    }
                }
            }
            this.row = _.orderBy(this.row,['q'],['asc']);
        }
    }
});



