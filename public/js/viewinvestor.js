var ref = firebase.database().ref('Finance/');
ref.on("value", function (snap) {
    const id = viewinvestor.id;
    let Deposit = snap.val().Deposit;
    let WithDraw = snap.val().WithDraw;
    let obj = [];
    for (let i in Deposit) {
        if (Deposit[i].u_id == id) {
            let o = {
                amount: Deposit[i].depo_fund,
                created: Deposit[i].depo_created,
                type: 0,
                status: Deposit[i].depo_status
            }
            obj.push(o);
        }
    };
    for (let j in WithDraw) {
        if (WithDraw[j].u_id == id) {
            let o = {
                amount: WithDraw[j].wd_amount,
                created: WithDraw[j].wd_created,
                type: WithDraw[j].wd_type,
                status: WithDraw[j].wd_status
            }
            obj.push(o);
        }
    };
    if (obj.length > 0) viewinvestor.dataHistory = _.orderBy(obj, ['created'], ['desc']);
});
const viewinvestor = new Vue({
    el: '#viewinvestor',
    data: {
        id: '',
        dataHistory: [],
        dataProfit: [],
        allData: [],
        fund: 0,
        allreturn: 0,
        allcom: 0,
        myData: [],
        row: [],
        stag: 0
    },
    mounted() {
        this.id = this.$refs.id.value;
        this.fetchData();
        this.FetchProfit();
    },
    computed: {
        total() {
            return this.fund + this.allreturn + this.stag;
        },
        moneyFundList() {
            let result = this.dataHistory.filter((q) => q.type == 0 && q.status == 2 || q.type == 3 && q.status == 2)
            return result;
        },
        moneyFundTotal() {
            let result = this.moneyFundList.reduce((sum,q)=> {
                if(q.type == 0){
                    return sum + parseInt(q.amount)
                } else if(q.type == 3){
                   return sum - parseInt(q.amount)
                }
            },0)
            return result;
        }
    },
    methods: {
        fetchData() {
            const vm = this;
            axios.get('/api/getAllmember').then((res) => {
                return vm.allData = res.data;
            });
        },
        FetchProfit() {
            const vm = this;
            let id = this.id;
            axios.get('/api/getProfit/' + id).then((res) => {
                return vm.dataProfit = _.orderBy(res.data, ['created_at'], ['desc']);
            });
        },
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
                console.log(e)
            }
        },
        ShowChild(id) {
            for (let i in this.row) {
                if (this.row[i].childof == id) {
                    this.row[i].show = !this.row[i].show;
                }
            }
        },
    },
    watch: {
        allData(val) {
            this.fund = parseFloat(val.find((q) => q.id == this.id).member_fund);
            this.stag = parseFloat(val.find((q) => q.id == this.id).member_stag);
            this.allreturn = parseFloat(val.find((q) => q.id == this.id).member_profit);
            this.allcom = parseFloat(val.find((q) => q.id == this.id).member_commission);
            this.myData = val.find((q) => q.id == this.id);
        },
        myData(val) {
            let q = 1;
            this.row = [];
            for (let i in this.allData) {
                if (val.id == this.allData[i].invite_id) {
                    let obj = {
                        q: q,
                        id: this.allData[i].id,
                        code: this.allData[i].member_code,
                        name: this.allData[i].name,
                        profit: this.allData[i].member_profit,
                        com: this.allData[i].member_commission,
                        img: this.allData[i].member_img,
                        cl: 'padd2',
                        childof: val.id,
                        show: true
                    }
                    this.row.push(obj);
                    q++;
                    for (let j in this.allData) {
                        if (this.allData[i].id == this.allData[j].invite_id) {
                            let obj = {
                                q: q,
                                id: this.allData[j].id,
                                code: this.allData[j].member_code,
                                name: this.allData[j].name,
                                profit: this.allData[j].member_profit,
                                com: this.allData[j].member_commission,
                                img: this.allData[j].member_img,
                                cl: 'padd4',
                                childof: this.allData[i].id,
                                show: false
                            }
                            this.row.push(obj);
                            q++;
                        }
                    }
                }
            }
            this.row = _.orderBy(this.row, ['q'], ['asc']);
        }
    }
})
