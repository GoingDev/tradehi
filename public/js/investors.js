Vue.component('modal', {
    template: '#modal-template'
})
const investor = new Vue({
    el : '#app',
    data : {
        investor : [],
        allfund : 0,
        filtext : ''
    },
    created(){
        this.FetchData();
    },
    computed : {
        filteredList() {
            return this.investor.filter((post) => {
                var alltext = post.name.toLowerCase()
              return alltext.includes(this.filtext.toLowerCase())
            })
        },
    },
    methods : {
        FetchData(data) {
            this.FetchMember();
        },
        FetchMember(){
            const vm = this;
            axios.get('/api/getAllmember').then((res)=> {
                let fund = 0;
                return vm.investor = res.data
            });
        },
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
              decimalCount = Math.abs(decimalCount);
              decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

              const negativeSign = amount < 0 ? "-" : "";

              let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
              let j = (i.length > 3) ? i.length % 3 : 0;

              return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
              console.log(e)
            }
        },
        EditRate(member_id, rate_id){
            appmodal.showModal = true;
            appmodal.dataChange.memberSelect = member_id;
            appmodal.dataChange.rateSeleted = rate_id;
            appmodal.FetchRateData();
        }
    },
    watch : {
        investor(val){
            this.allfund = val.reduce((sum,q)=>sum+parseFloat(q.member_fund),0);
        }
    }
});

const appmodal = new Vue({
    el: '#appmodal',
    data: {
        showModal: false,
        dataGroupRow: [],
        dataChange:{
            memberSelect: '',
            rateSeleted: '1'
        }
    },
    created(){
    },
    methods : {
        FetchRateData(data) {
            const vm = this;
            axios.get('/api/setting').then(function (res) {
                let data = res.data;
                vm.dataNormalRow = [];
                vm.dataGroupRow = [];
                for (let i in data) {
                    vm.dataGroupRow.push(data[i]);
                }
            });
        },
        UpdateMemberRate(){
            const vm = this;
            axios.post('/api/update_member_rate', vm.dataChange).then((res)=>{
                swal({
                    title : 'Success',
                    text : 'Update rate success',
                    type : 'success'
                })
            });
            investor.FetchData();
            vm.showModal = false;
        }
    }
});
