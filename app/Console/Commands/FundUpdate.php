<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\User;
use App\Member;
use DB;
class FundUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fund:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update stag to fund';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $member = User::join('members','members.u_id','=','users.id')->get();
        foreach($member as $row){
            if($row->stag_date != null){
                $now = Carbon::now();
                $date = Carbon::parse($row->stag_date);
                $diff = $date->diffInDays($now,false);
                if($row->member_rate == 4){
                    $member = DB::table('members')->where('u_id',$row->id)->increment('member_fund',round(floatval($row->member_stag),2));
                    $member = DB::table('members')->where('u_id',$row->id)->update(['member_stag'=>0,'stag_date'=>null]);
                }
                if($row->member_rate == 3){
                    if($diff >= 1){
                        $member = DB::table('members')->where('u_id',$row->id)->increment('member_fund',round(floatval($row->member_stag),2));
                        $member = DB::table('members')->where('u_id',$row->id)->update(['member_stag'=>0,'stag_date'=>null]);
                    }
                }
                else if($row->member_rate == 1){
                    if($diff >= 3){
                        $member = DB::table('members')->where('u_id',$row->id)->increment('member_fund',round(floatval($row->member_stag),2));
                        $member = DB::table('members')->where('u_id',$row->id)->update(['member_stag'=>0,'stag_date'=>null]);
                    }
                }
            }
        }
    }
}

//Pianporn 135000 old 85000 stag 50000 // 07-01-2019
