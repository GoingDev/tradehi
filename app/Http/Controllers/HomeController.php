<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Member;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user-dashboard');
    }
    public function adminindex()
    {
        return view('admin-dashboard');
    }
    public function usermanagement()
    {
        return view('usermanagement');
    }
    public function EditProfile(){
        $Itme = User::join('members','members.u_id', '=' , 'users.id')->where('id',Auth::user()->id)->get();
        return view('editprofile')->with(['myprofile'=>$Itme[0]]);
    }
    public function Updateprofile(Request $request){
        $id = Auth::user()->id;
        $email = $request->email;
        $name = $request->name;
        $address = $request->address;
        $tel = $request->tel;
        $file = $request->main_file;
        $auto = $request->auto;
        if($request->hasFile('main_file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().base64_encode($ex1[0]).".jpg";
            $file->move(public_path('picture'), $fileName);
            $img = $fileName;
            $user = User::where('id',$id)->update(['email'=>$email,'name'=>$name]);
            $member_id = Member::where('u_id',$id)->update(['autotransfer'=>$auto,'member_address'=>$address,'member_tel'=>$tel,'member_img'=>$img]);
        }
        else {
            $user = User::where('id',$id)->update(['email'=>$email,'name'=>$name]);
            $member_id = Member::where('u_id',$id)->update(['autotransfer'=>$auto,'member_address'=>$address,'member_tel'=>$tel]);
        }
        return redirect()->back()->with('message', ' Change your profile success !');
    }
    public function Setting()
    {
        return view('setting');
    }
    public function Investors()
    {
        return view('investorsmanagement');
    }
    public function Contact()
    {
        return view('contact');
    }
    public function historyfund()
    {
        return view('historyfund');
    }
    public function Profit()
    {
        return view('Profit');
    }
    public function viewallRequest(){
        return view('viewallRequests');
    }
    public function Allnotic(){
        return view('layouts.viewnotification');
    }
    public function Partners(){
        return view('Partners');
    }
    public function viewinvestor($id){
        return view('viewinvestor')->with(['id'=>$id]);
    }
}
