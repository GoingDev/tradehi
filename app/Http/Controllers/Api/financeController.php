<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use DB;
use App\ProfitHistory;
use App\User;
use App\Member;
use App\FirebaseConnection;
use App\ProfitRate;
class financeController extends Controller
{
    //
    /*
    public function index(){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/tradehi-f2461-firebase-adminsdk-ugkq4-3ea719f67c.json');

        $firebase = (new Factory)

            ->withServiceAccount($serviceAccount)

            ->withDatabaseUri('https://tradehi-f2461.firebaseio.com/')

            ->create();

            $database = $firebase->getDatabase();

            $newPost = $database

            ->getReference('blog/posts')

            ->push([

            'title' => 'Post title',

            'body' => 'This should probably be longer.'

            ]);

        //$newPost->getKey(); // => -KVr5eu8gcTv7_AHb-3-

        //$newPost->getUri(); // => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-

        //$newPost->getChild('title')->set('Changed post title');

        //$newPost->getValue(); // Fetches the data from the realtime database

        //$newPost->remove();

        echo"<pre>";

        print_r($newPost->getvalue());

    }
    */
    public function AddFund(Request $request){
        $client = new Client();
        $code = $client->formatedId($alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', $size = 12);
        $id = $request->id;
        $fund = $request->fund;
        $file = $request->file;
        $now = Carbon::now();
        $dt = $now->toDateTimeString();
        $img = '' ;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().base64_encode($ex1[0]).".jpg";
            $file->move(public_path('slip'), $fileName);
            $img = $fileName;
        }
        $find = User::find($id);
        $database = FirebaseConnection::FBConnection();
        $newDeposit = $database->getReference('Finance/Deposit')
                                ->push([
                                        'u_id' => $id,
                                        'depo_fund' => round(floatval($fund),2),
                                        'depo_codeorder' => $code,
                                        'depo_status' => 1,
                                        'depo_verifyimg' => $img,
                                        'depo_created' => $dt
                                        ]);


        $newNotic = $database->getReference('Notic')
                                ->push([
                                        'notic_created' => $dt,
                                        'notic_status' => 0,
                                        'notic_seen' => 1,
                                        'notic_uid' => $id,
                                        'notic_name' => $find->name,
                                        'notic_action' => 'ได้ฝากเงินจำนวน ' .round(floatval($fund),2). ' baht',
                                        'notic_link' => '/admin',
                                        'notic_type' => 1
                                        ]);
    }
    public function WithDraw(Request $request){
        $id = $request->id;
        $amount = $request->amount;
        $type = $request->type;
        $client = new Client();
        $code = $client->formatedId($alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', $size = 12);
        $now = Carbon::now();
        $dt = $now->toDateTimeString();
        $database = FirebaseConnection::FBConnection();
        $newPost = $database->getReference('Finance/WithDraw')
                                ->push([
                                        'u_id' => $id,
                                        'wd_amount' => round(floatval($amount),2),
                                        'wd_codeorder' => $code,
                                        'wd_status' => 1,
                                        'wd_type' => $type,
                                        'wd_created' => $dt
                                        ]);
        $notic_text = '';
        if($type == 1){
            $notic_text = "ได้แปลงผลตอบแทนเป็นทุนจำนวน";
        }
        else if($type == 2){
            $notic_text = "ได้ถอนผลตอบแทนจำนวน";
        }
        else if($type == 3){
            $notic_text = "ได้ถอนต้นทุนจำนวน";
        }
        else if($type == 4){
            $notic_text = "ได้ถอนคอมมิสชั่นจำนวน";
        }
        $find = User::find($id);
        $newNotic = $database->getReference('Notic')
                                ->push([
                                        'notic_created' => $dt,
                                        'notic_status' => 0,
                                        'notic_seen' => 1,
                                        'notic_uid' => $id,
                                        'notic_action' => $notic_text." ".round(floatval($amount),2). ' baht',
                                        'notic_link' => '/admin',
                                        'notic_name' => $find->name,
                                        'notic_type' => 2,
                                        ]);

    }
    public function ApproveRequests(Request $request){
        $update = $request->get('update');
        $json = json_decode($update);
        $key = $request->get('key');
        $now = Carbon::now();
        $dt = $now->toDateTimeString();
        $database = FirebaseConnection::FBConnection();
        $user = DB::table('members')->where('u_id',$json->u_id)->first();
        $member_rate = $user->member_rate;
        if(isset($json->wd_type)){
            $type = $json->wd_type;
            switch ($type){
                case "1" :
                    $Checkamount = DB::table('members')->select('member_profit')->where('u_id',$json->u_id)->get();
                    if($json->wd_amount > $Checkamount[0]->member_profit){
                        return  response()->json(['success'=>'Profit has not enough']);
                    }
                    $member = DB::table('members')->where('u_id',$json->u_id)->decrement('member_profit',round(floatval($json->wd_amount),2));
                    $member = DB::table('members')->where('u_id',$json->u_id)->increment('member_fund',round(floatval($json->wd_amount),2));
                    break;
                case "2" :
                    $Checkamount = DB::table('members')->select('member_profit')->where('u_id',$json->u_id)->get();
                    if($json->wd_amount > $Checkamount[0]->member_profit){
                        return  response()->json(['success'=>'Profit has not enough']);
                    }
                    $member = DB::table('members')->where('u_id',$json->u_id)->decrement('member_profit',round(floatval($json->wd_amount),2));
                    break;
                case "3" :
                    $Checkamount = DB::table('members')->select('member_fund')->where('u_id',$json->u_id)->get();
                    if($json->wd_amount > $Checkamount[0]->member_fund){
                        return  response()->json(['success'=>'Fund has not enough']);
                    }
                    $member = DB::table('members')->where('u_id',$json->u_id)->decrement('member_fund',round(floatval($json->wd_amount),2));
                    break;
                case "4" :
                    $Checkamount = DB::table('members')->select('member_commission')->where('u_id',$json->u_id)->get();
                    if($json->wd_amount > $Checkamount[0]->member_commission){
                        return  response()->json(['success'=>'Commission has not enough']);
                    }
                    $member = DB::table('members')->where('u_id',$json->u_id)->decrement('member_commission',round(floatval($json->wd_amount),2));
                    break;

            }
            $updates = [
                'Finance/WithDraw/'.$key => $json,
            ];
            $database->getReference()
                                ->update($updates);
            $newNotic = $database->getReference('Notic')
                                ->push([
                                        'notic_created' => $dt,
                                        'notic_status' => 0,
                                        'notic_seen' => $json->u_id,
                                        'notic_uid' => 1,
                                        'notic_name' => 'Admin',
                                        'notic_action' => 'อนุมัติการถอนเงินเรียบร้อยแล้ว',
                                        'notic_link' => '/History',
                                        'notic_type' => 3
                                        ]);
        }
        else{
            $updates = [
                'Finance/Deposit/'.$key => $json,
            ];
            $database->getReference()
                                ->update($updates);
            if($member_rate == 4){
                $member = DB::table('members')->where('u_id',$json->u_id)->increment('member_fund',round(floatval($json->depo_fund),2));
            }
            else {
                $member = DB::table('members')->where('u_id',$json->u_id)->increment('member_stag',round(floatval($json->depo_fund),2));
                $member = DB::table('members')->where('u_id',$json->u_id)->update(['stag_date'=>$dt]);
            }
            $newNotic = $database->getReference('Notic')
                                ->push([
                                        'notic_created' => $dt,
                                        'notic_status' => 0,
                                        'notic_seen' => $json->u_id,
                                        'notic_uid' => 1,
                                        'notic_name' => 'Admin',
                                        'notic_action' => 'อนุมัติการฝากเงินเรียบร้อยแล้ว',
                                        'notic_link' => '/History',
                                        'notic_type' => 4
                                        ]);
        }
        return response()->json(['success'=>'done']);
    }
    public function addProfit(Request $request){
        $amount = $request->get('amount');
        $now = Carbon::now();
        $total = $request->get('total');
        $dtx = $now->toDateTimeString();
        $dt = explode(" ",$now->toDateTimeString());
        $database = FirebaseConnection::FBConnection();
        $member = User::join('members','members.u_id','=','users.id')->get();
        $allreturn = 0;
        $allbalance = 0;
        $allcom = 0;
        foreach($member as $row){
            if($row->id != 1){
                $find_rate = ProfitRate::where('rate_id',$row->member_rate)->get();
                $rate_return = $find_rate[0]->rate_return;
                $diff_return = 100-$rate_return;
                $dividend = (($row->member_fund/$total)*100);
                $profit = $this->CalFinance($amount,$dividend);
                $return = $this->CalFinance($profit,intval($rate_return));
                $balance = $this->CalFinance($profit,intval($diff_return));
                $idlevel1 = $row->invite_id;
                if($idlevel1!=0){
                    $comlavel1 = $this->CalFinance($balance,20);
                    $balance = floatval($balance) - floatval($comlavel1);
                    $allcom += $comlavel1;
                    $Upcommission = Member::where('u_id',$idlevel1)->increment('member_commission',round(floatval($comlavel1),2));
                    $findlevel2 = User::where('id',$idlevel1)->get();
                    $idlevel2 = $findlevel2[0]->invite_id;
                    if($idlevel2 != 0){
                        $comlavel2 = $this->CalFinance($balance,10);
                        $balance = floatval($balance) - floatval($comlavel2);
                        $allcom += $comlavel2;
                        $Upcommission = Member::where('u_id',$idlevel2)->increment('member_commission',round(floatval($comlavel2),2));
                    }
                }
                if($row->autotransfer == 0){
                    $Upprofit = Member::where('u_id',$row->id)->increment('member_profit',round(floatval($return),2));
                }
                else if($row->autotransfer == 1){
                    $Upprofit = Member::where('u_id',$row->id)->increment('member_fund',round(floatval($return),2));
                }
                $Upprofit = Member::where('u_id',$row->id)->increment('member_balance',round(floatval($balance),2));
                $profitbyMember = new ProfitHistory;
                $profitbyMember->u_id = $row->id;
                $profitbyMember->profit = round($return,2);
                $profitbyMember->balance = round($balance,2);
                $profitbyMember->status = 1;
                $profitbyMember->save();
                $allreturn += round($return,2);
                $allbalance += round($balance,2);
                if($return > 0){
                    $newNotic = $database->getReference('Notic')
                                ->push([
                                        'notic_created' => $dtx,
                                        'notic_status' => 0,
                                        'notic_seen' => $row->id,
                                        'notic_uid' => 1,
                                        'notic_name' => 'Admin',
                                        'notic_action' => 'ได้รับ Profit จำนวน '.round($return,2). " บาท",
                                        'notic_link' => '/Profit',
                                        'notic_type' => 5
                                        ]);
                }
            }
        }
        $newPost = $database->getReference('Finance/Profit')
                                ->push([
                                        'amount' => $amount,
                                        'date' => $dt[0],
                                        'time' => $dt[1],
                                        'return' => $allreturn,
                                        'balance' => $allbalance,
                                        'com' => $allcom
                                        ]);
        return response()->json(['success'=>'done']);

    }
    public function CalFinance($amount , $rate){
        return round(($amount*$rate)/100,2);
    }
}



//UPDATE `members` SET `member_profit`=0,`member_balance`=0 WHERE 1
//type 0 = register , type 1 = deposit , type 2 = withdraw , type 3 = appwd , type 4 = appdepo , type 5 = profit , type 6 = rejectwd , type 7 = rejectdepo
//status 0 = ยังไม่เห็น , status 1 = เห็นเเล้วเเต่ยังไม่อ่าน , status 2 = อ่านเเล้ว
