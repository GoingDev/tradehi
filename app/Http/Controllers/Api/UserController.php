<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;
use DB;
use App\User;
use App\Member;
use App\ProfitHistory;
class UserController extends Controller
{
    //
    public function getAlluser(){
        $user = User::all();
        return response()->json($user);
    }
    public function Deluser($id){
        $deluser = User::where('id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function Edituser(Request $request){
        $data = $request->input();
        $client = new Client();
        $update = User::where('id',$data['id'])->update(['email'=>$data['email'],'name'=>$data['name'],'status'=>3]);
        $u_id = $data['id'];
        $code = $client->formatedId($alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', $size = 12);
        $member = new Member;
        $member->u_id = $u_id;
        $member->member_code = $code;
        $member->save();
        return response()->json(['success','done']);
    }
    public function getAllmember(){
        $user = User::join('members','members.u_id','=','users.id')
        ->join('profit_rates', 'members.member_rate', '=', 'profit_rates.rate_id')
        ->orderBy('members.member_fund', 'desc')
        ->get();
        return response()->json($user);
    }
    public function Delmember($id){
        $findu_id = Member::where('member_id',$id)->select('u_id')->get();
        $u_id = $findu_id[0]->u_id;
        $deluser = User::where('id',$u_id)->delete();
        $delmember = Member::where('member_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
    public function Editmember(Request $request){
        $member_id = $request->id;
        $u_id = $request->u_id;
        $file = $request->file;
        $address  = $request->address;
        $tel  = $request->tel;
        $fund  = $request->fund;
        $name  = $request->name;
        $email  = $request->email;
        $bookbank_name  = $request->bookbank_name;
        $passport  = $request->passport;
        $bookbank_num  = $request->bookbank_num;
        $user = User::where('id',$u_id)->update(['name'=>$name,'email'=>$email,'passport'=>$passport,
                                                    'bankname'=>$bookbank_name,'banknum'=>$bookbank_num]);
        $img = null ;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().base64_encode($ex1[0]).".jpg";
            $file->move(public_path('picture'), $fileName);
            $img = $fileName;
        }

        $member = Member::where('member_id',$member_id)->update(['member_address'=>$address,'member_tel'=>$tel,
                                                            'member_img'=>$img,'member_fund'=>floatval($fund)]);
        return response()->json(['success'=>'done']);
    }
    public function testnanoid(){
        $client = new Client();
        return $client->formatedId($alphabet = '0123456789abcdefg', $size = 12);
    }
    public function getmember($id){
        $member = Member::where('u_id',$id)
                                            ->join('profit_rates', 'members.member_rate', '=', 'profit_rates.rate_id')
                                            ->get();
        return response()->json($member);
    }
    public function getProfit($id){
        $profit = ProfitHistory::where('u_id',$id)->get();
        return response()->json($profit);
    }
    public function UpdateMemberRate(Request $request){
        $data = $request->input();
        $member = Member::where('member_id',$data['memberSelect'])->update(['member_rate' =>$data['rateSeleted']]);

        return response()->json(['success'=>'done']);
    }
}
