<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\FirebaseConnection;
use Carbon\Carbon;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'passport' => 'required|unique:users',
            'bank_num' => 'required|numeric'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $passport = $data['passport_img'];
        $ex1 = explode(".",$passport->getClientOriginalName());
        $passportName = time().base64_encode($ex1[0]).".jpg";
        $upload_passport = $passport->move(public_path('passport'),$passportName);

        $bookbank = $data['bookbank_img'];
        $ex2 = explode(".",$bookbank->getClientOriginalName());
        $bookbankName = time().base64_encode($ex2[0]).".jpg";
        $upload_bookbank = $bookbank->move(public_path('bookbank'),$bookbankName);

        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->status = 2;
        $user->passport_img = $passportName;
        $user->bookbank_img = $bookbankName;
        $user->passport = $data['passport'];
        $user->bankname = $data['bank_name'];
        $user->banknum = $data['bank_num'];
        $user->invite_id = $data['invite_id'];
        $user->remember_token = $data['_token'];
        $user->save();
        if(isset($user->id)){
        $now = Carbon::now();
        $dt = $now->toDateTimeString();
        $database = FirebaseConnection::FBConnection();
        $newNotic = $database->getReference('Notic')
                                ->push([
                                        'notic_created' => $dt,
                                        'notic_status' => 0,
                                        'notic_seen' => 1,
                                        'notic_uid' => 0,
                                        'notic_name' => $data['name'],
                                        'notic_action' => 'คุณ ' . $data['name'] . ' ได้สมัครสมาชิกเข้ามาใหม่',
                                        'notic_link' => '/usermanagement',
                                        'notic_type' => 0
                                        ]);
        }
        return $user;
    }
}
