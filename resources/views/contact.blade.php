@extends('layouts.layout')

@section('content')
<div class="content-area">
    <div class="row">
        <div class="col-12">
            <h1 class="title">
                Contact  
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">
                        ฝากเงินเข้าบัญชี
                        <strong style="color: #00a850"> ธนาคารกสิกรไทย</strong>
                     </h1>
                    <h2>
                        โดยการโอนมาที่
                        <strong>9802007490</strong>  
                    </h2>
                    <h3>
                        ชื่อบัญชี
                        <strong>นายสุรสิทธิ์ คำยันต์ </strong>
                    </h3>
                </div>
                </div>
        </div>
    </div>
</div>

@endsection
