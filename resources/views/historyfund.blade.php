@extends('layouts.layout')

@section('content')
<style>
    th {
        background-color: rgba(0, 0, 0, .03);
    }

    table td {
        vertical-align: middle !important;
        padding: 6px;
    }

    table p {
        margin: 0px;
    }
    td,th{
        text-align: center;
    }

</style>
<div class="content-area -admin" id="history">
    <div v-cloak class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-usd-circle"></i>&nbsp;ประวัติการเงิน</h3>
                    <div v-show="historyLoader" class="ml-auto">
                        <i class="fal fa-spinner-third spin-ani mr-2"></i>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-sm bg-white">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">#</th>
                                <th scope="col">Time</th>
                                <th scope="col">Action</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data, index) in dataDip">
                                <td scope="row" class="text-center">@{{
                                    dataDip.length - index
                                    }}</td>
                                <th class="text-center">@{{data.created}}</th>
                                <td>
                                    <p v-if="data.type == 0">
                                        ฝากเงิน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 1">
                                        แปลงเป็นทุน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 2">
                                        ถอนผลตอบแทน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 3">
                                        ถอนทุน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 4">
                                        ถอนคอมมิสชั่น @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else>
                                        ไม่มี type
                                    </p>
                                </td>
                                <td class="text-center">
                                    <p class="text-warning" v-if="data.status == 1">
                                        Wating
                                    </p>
                                    <p class="text-success" v-else-if="data.status == 2">
                                        Approved
                                    </p>
                                    <p class="text-danger" v-else-if="data.status == 3">
                                        Rejected
                                    </p>
                                    <p v-else>
                                        ไม่มีสถานะ
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('/js/history.js')}}"></script>
@endsection
