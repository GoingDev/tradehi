@extends('layouts.layout')

@section('content')
<style>

    i {
        margin: 5px;
        cursor: pointer;
        font-size: 20px;
    }
    .table-transaction tr > td:nth-last-child(1) {
        width: 100%;
        display: inline-flex;
        justify-content: flex-end;
    }
    .btn{
        margin-top:10px;
        margin-left:5px;
    }
    .modal-body{
        margin-bottom: 0px;
    }
    .table{
        margin-bottom : 0px;
    }
    .table td, .table th {
        vertical-align: middle;
    }



    .timeline-item {
        background: #fff;
        border: 1px solid;
        border-color: #e5e6e9 #dfe0e4 #d0d1d5;
        border-radius: 3px;
        padding: 12px;
        margin: 0 auto;
        width: 100%;
    }
    @keyframes placeHolderShimmer{
        0%{
            background-position: -468px 0
        }
        100%{
            background-position: 468px 0
        }
    }

    .animated-background {
        animation-duration: 1s;
        animation-fill-mode: forwards;
        animation-iteration-count: infinite;
        animation-name: placeHolderShimmer;
        animation-timing-function: linear;
        background: #f6f7f8;
        background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
        background-size: 800px 104px;
        position: relative;
    }
    [v-cloak] {
        display: none;
    }

</style>
<div class="content-area -admin" id="viewall">
    <h3 class="title"><i class="fal fa-users"></i>Transaction Requests</h3>
    <div v-cloak class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h3 class="title">Waiting</h3>
                    <div class="ml-auto">
                        <i class="fal fa-spinner-third spin-ani mr-2 mt-3" v-show="!Loaddata"></i>
                        <button :disabled="pending" class="btn btn-sm btn btn-primary mt-0" style="color:white;"
                            v-on:click="AllTransfer()">Approve all Transfer</button>
                        <a href="/admin" class="btn btn-sm btn btn-secondary mt-0" style="color:white;">Back</a>
                    </div>
                </div>
                <div class="table-responsive mt-2" v-cloak>
                    <table class="table table-bordered bg-white">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">ID</th>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Type</th>
                                <th scope="col">Investor</th>
                                <th scope="col">Bank</th>
                                <th scope="col">Status</th>
                                <th>Other</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(y,index) in dataWaiting">
                                <th scope="row">@{{y.code}}</th>
                                <th>@{{y.date}}</th>
                                <td>@{{y.time}}</td>
                                <td class="text-success">@{{formatMoney(y.amount)}} Baht</td>
                                <td>@{{y.type}}</td>
                                <td><a :href="'/viewinvestor/'+y.u_id">@{{y.investor}}</a></td>
                                <td>
                                    <span v-if="y.status == 1" style="color:orange;">Waiting</span>
                                </td>
                                <td>
                                    <i class="fas fa-university" data-toggle="tooltip" :title="y.bank"></i>
                                </td>
                                <td class="text-right">
                                    <span v-if=" y.status == 1 ">
                                        <i v-show=" y.type == 'Deposit' " class="fas fa-image" v-on:click="View(index)"
                                            style="color:grey;"></i>
                                        <i class="fas fa-check" v-on:click="Approve(index,y.type)" style="color:grey;"></i>
                                        <i class="fas fa-times" v-on:click="Reject(index,y.type)" style="color:grey;"></i>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-12 text-sm-right">
                        WD Total @{{formatMoney(wdtotal)}} บาท &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div v-cloak class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title">Approved</h3>
                    <div class="ml-auto">
                        <i class="fal fa-spinner-third spin-ani mr-2 mt-3" v-show="!Loaddata"></i>
                        <div class="row">
                            <div class="d-flex">
                                <input type="text" class="form-control" placeholder="Filter" v-model="filtext">&nbsp;
                                <a href="/admin" class="btn btn-sm btn btn-secondary mt-0" style="color:white;">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive mt-2" v-cloak>
                    <table class="table table-bordered bg-white">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">ID</th>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Type</th>
                                <th scope="col">Investor</th>
                                <th scope="col">Bank</th>
                                <th scope="col">Status</th>
                                <th scope="col">Updated</th>
                                <th>Other</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(y,index) in filteredList">
                                <th scope="row">@{{y.code}}</th>
                                <th>@{{y.date}}</th>
                                <td>@{{y.time}}</td>
                                <td class="text-success">@{{formatMoney(y.amount)}} Baht</td>
                                <td>@{{y.type}}</td>
                                <td><a :href="'/viewinvestor/'+y.u_id">@{{y.investor}}</a></td>
                                <td>
                                    <span v-if="y.status == 2" style="color:green;">Approved</span>
                                    <span v-else-if="y.status == 3" style="color:red;">Rejected</span>
                                </td>
                                <td>
                                    <i class="fas fa-university" data-toggle="tooltip" :title="y.bank"></i>
                                </td>
                                <td>@{{y.updated}}</td>
                                <td class="text-right">
                                    <span v-if=" y.status == 2 ">
                                        <i v-show=" y.type == 'Deposit' " class="fas fa-image" v-on:click="View2(index)"
                                            style="color:grey;"></i>
                                        <i class="far fa-check-square" style="color:grey;"></i>
                                    </span>
                                    <span v-else-if=" y.status == 3 ">
                                        <i class="far fa-times-square" style="color:grey;"></i>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- appmodal -->
<div id="appmodal" v-cloak>
    <!-- use the modal component, pass in the prop -->
    <modal v-if="showModal" @close="showModal = false">
        <!--
                you can use custom content here to overwrite
                default content
            -->
        <h3 slot="header">Verify Img</h3>
        <h3 slot="body"><img class="img_modal" :src="img_modal"></h3>
    </modal>
</div>
<!--script-->
<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="$emit('close')">
            <div class="modal-container">

                <div class="modal-header">
                <slot name="header">
                    default header
                </slot>
                </div>

                <div class="modal-body">
                <slot name="body">
                    default body
                </slot>
                </div>
                <!--
                <div class="modal-footer">
                <slot name="footer">
                    default footer
                </slot>
                </div>-->
            </div>
            </div>
        </div>
        </transition>
    </script>
<script src="{{asset('/js/viewallRequest.js?12')}}"></script>
@endsection
