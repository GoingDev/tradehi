@extends('layouts.layout')

@section('content')
<style>
    .img_profile {
        width: 128px;
        height: 128px;
        cursor: pointer;
    }

    .btndeposit>.btn {
        margin-left: 5px;
    }

    .modal-body {
        padding: 15px 10px 25px !important;
    }

    .modal-container {
        padding: 0 !important;
    }

    .modal-header {
        border-radius: 0px;
    }

    .modal-header h3 {
        color: white !important;
        margin-bottom: 0px !important;
    }


    table p {
        margin: 0px;
    }

    [v-clock] {
        display: block;
    }

    .lds-spinner {
        color: #478bf6 !important;
        display: inline-block;
        position: absolute;
        top: 20px;
        right: 20px;
        width: 64px;
        height: 64px;
        z-index: 1000px;
    }

    .lds-spinner div {
        transform-origin: 32px 32px;
        animation: lds-spinner 1.2s linear infinite;
    }

    .lds-spinner div:after {
        content: " ";
        display: block;
        position: absolute;
        top: 3px;
        left: 29px;
        width: 5px;
        height: 14px;
        border-radius: 20%;
        background: #478bf6;
    }

    .lds-spinner div:nth-child(1) {
        transform: rotate(0deg);
        animation-delay: -1.1s;
    }

    .lds-spinner div:nth-child(2) {
        transform: rotate(30deg);
        animation-delay: -1s;
    }

    .lds-spinner div:nth-child(3) {
        transform: rotate(60deg);
        animation-delay: -0.9s;
    }

    .lds-spinner div:nth-child(4) {
        transform: rotate(90deg);
        animation-delay: -0.8s;
    }

    .lds-spinner div:nth-child(5) {
        transform: rotate(120deg);
        animation-delay: -0.7s;
    }

    .lds-spinner div:nth-child(6) {
        transform: rotate(150deg);
        animation-delay: -0.6s;
    }

    .lds-spinner div:nth-child(7) {
        transform: rotate(180deg);
        animation-delay: -0.5s;
    }

    .lds-spinner div:nth-child(8) {
        transform: rotate(210deg);
        animation-delay: -0.4s;
    }

    .lds-spinner div:nth-child(9) {
        transform: rotate(240deg);
        animation-delay: -0.3s;
    }

    .lds-spinner div:nth-child(10) {
        transform: rotate(270deg);
        animation-delay: -0.2s;
    }

    .lds-spinner div:nth-child(11) {
        transform: rotate(300deg);
        animation-delay: -0.1s;
    }

    .lds-spinner div:nth-child(12) {
        transform: rotate(330deg);
        animation-delay: 0s;
    }

    @keyframes lds-spinner {
        0% {
            opacity: 1;
        }

        100% {
            opacity: 0;
        }
    }

</style>
<div id="app" v-cloak>
    <div class="content-area" style="position:relative;">
        <div class="row" v-show="loadPage == false">
            <div class="col-xl-9">
                <!-- My Fund -->
                <div class="my-fund card border-success position-relative">
                    <div class="card-header d-flex flex-column flex-md-row justify-content-between">
                        <div class="d-flex justify-content-between">
                            <h3 class="title">
                                <i class="fas fa-usd-circle"></i> การลงทุนของฉัน
                                <span class="text-success">เติบโต @{{formatMoney(grow)}}%</span>
                            </h3>
                        </div>
                        @if(Auth::user()->id == 1)
                        @else
                        <div class="d-flex align-items-center w-md-auto  mt-2 mt-md-0">
                            <button href="#" class="btn btn-sm btn-success mr-2 flex-grow-1"
                                v-on:click="DepositPanel()">
                                <i class="fal fa-plus"></i> ฝากเงิน
                            </button>
                            <button href="#" class="btn btn-sm btn-secondary flex-grow-1"
                                v-on:click="WithDrawPanel()">ถอนเงิน</button>
                        </div>
                        @endif
                    </div>

                    <div class="card-body fund-summary">
                        <div class="row">

                            <div class="col-12 col-md-3">
                                <div class="d-flex mb-2 justify-content-md-center fade-icon">
                                    <i class="fa-2x fal fa-redo-alt"></i>
                                    <div>
                                        <p class="amount text-success">
                                            @{{formatMoney(allreturn)}} บาท
                                        </p>
                                        <p class="small">
                                            ผลตอบแทน
                                        </p>
                                    </div>

                                </div>
                            </div>

                            <div class="col-12 col-md-3">
                                <div class="d-flex mb-2 justify-content-md-center fade-icon">
                                    <i class="fa-2x fas fa-usd-circle"></i>
                                    <div>
                                        <p class="amount">
                                            @{{formatMoney(fund+stag)}} บาท
                                        </p>
                                        <p class="small">
                                            ทุนเริ่มต้น
                                        </p>
                                    </div>

                                </div>
                            </div>

                            <div class="col-12 col-md-3">
                                <div class="d-flex mb-2 justify-content-md-center fade-icon">
                                    <i class="fa-2x fal fa-usd-circle"></i>
                                    <div>
                                        <p class="amount">
                                            @{{formatMoney(total)}} บาท
                                        </p>
                                        <p class="small">
                                            รวมทั้งหมด
                                        </p>
                                    </div>

                                </div>
                            </div>

                            <div class="col-12 col-md-3">
                                <div class="d-flex mb-2 justify-content-md-center fade-icon">
                                    <i class="fa-2x fal fal fa-users"></i>
                                    <div>
                                        <p class="amount">
                                            @{{formatMoney(allcom)}} บาท
                                        </p>
                                        <p class="small">
                                            คอมมิสชั่น
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- My Fund -->


                <!-- Fund list -->
                {{-- <div class="my-investment card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h3 class="title"><i class="fal fa-usd-circle"></i> เงินฝาก</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-sm bg-white">
                            <thead>
                                <tr>
                                    <th scope="col">วันและเวลา</th>
                                    <th scope="col">รายการคำขอ</th>
                                    <th scope="col">สถานะ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(data, index) in moneyFundList">
                                    <th>@{{data.created}}</th>
                                    <td>
                                        <p v-if="data.type == 0">
                                            ฝากเงิน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 1">
                                            แปลงเป็นทุน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 2">
                                            ถอนผลตอบแทน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 3">
                                            ถอนทุน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 4">
                                            ถอนคอมมิสชั่น @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else>
                                            ไม่มี type
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-warning" v-if="data.status == 1">
                                            Wating
                                        </p>
                                        <p class="text-success" v-else-if="data.status == 2">
                                            Approved
                                        </p>
                                        <p class="text-danger" v-else-if="data.status == 3">
                                            Rejected
                                        </p>
                                        <p v-else>
                                            ไม่มีสถานะ
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">จำนวนเงินฝากคงเหลือ</td>
                                    <td class="text-center" colspan="2">@{{formatMoney(moneyFundTotal)}} บาท</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> --}}

                <!-- History -->
                <div class="my-investment card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h3 class="title"><i class="fal fa-usd-circle"></i> สถานะรายการคำขอ</h3>
                        <div class="ml-auto d-flex align-items-center">
                            <div v-show="">
                                <i class="mr-2 fal fa-spinner-third spin-ani"></i>
                            </div>
                            <a href="{{url('/History')}}" class="btn btn-sm btn-outline-secondary">ดูทั้งหมด</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-sm bg-white">
                            <thead>
                                <tr>
                                    <th scope="col">วันและเวลา</th>
                                    <th scope="col">รายการคำขอ</th>
                                    <th scope="col">สถานะ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(data, index) in dataDip.slice(0,4)">
                                    <th>@{{data.created}}</th>
                                    <td>
                                        <p v-if="data.type == 0">
                                            ฝากเงิน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 1">
                                            แปลงเป็นทุน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 2">
                                            ถอนผลตอบแทน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 3">
                                            ถอนทุน @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else-if="data.type == 4">
                                            ถอนคอมมิสชั่น @{{formatMoney(data.amount)}} บาท
                                        </p>
                                        <p v-else>
                                            ไม่มี type
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-warning" v-if="data.status == 1">
                                            Wating
                                        </p>
                                        <p class="text-success" v-else-if="data.status == 2">
                                            Approved
                                        </p>
                                        <p class="text-danger" v-else-if="data.status == 3">
                                            Rejected
                                        </p>
                                        <p v-else>
                                            ไม่มีสถานะ
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>จำนวนเงินฝาก</td>
                                    <td colspan="2">@{{formatMoney(moneyFundTotal)}} บาท</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- My Investment -->

                <div class="my-investment card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h3 class="title"><i class="fal fa-chart-line"></i> รายการผลตอบแทน</h3>

                        <div class="ml-auto d-flex align-items-center">
                            <div v-show="allprofitLoader">
                                <i class="mr-2 fal fa-spinner-third spin-ani"></i>
                            </div>
                            <a href="{{url('/Profit')}}" class="btn btn-sm btn-outline-secondary">ดูทั้งหมด</a>
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">วันและเวลา</th>
                                    <th scope="col">ผลตอบแทน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="x in allprofit.slice(0,4)">
                                    <th scope="row">@{{x.created_at}}</th>
                                    <td class="text-success">@{{formatMoney(x.profit)}} บาท</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="my-rank card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h3 class="title"><i class="fal fa-users"></i> Investors Rank</h3>
                        </div>

                    </div>
                    <div class="table table-responsive">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th class="text-center">Rank</th>
                                    <th class="text-center">#</th>
                                    <th>ID</th>
                                    <th>NAME</th>
                                    <th>CLASS</th>
                                    <th>RATIO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(inv,index) in invester">
                                    <td class="text-center">@{{(index+1)}}</td>
                                    <td class="text-center">
                                        <img v-if=" inv.member_img != null " :src="'/picture/'+inv.member_img"
                                            class="avatar" alt="">
                                        <img v-else src="/picture/avatar.jpg" class="avatar" alt="">
                                    </td>
                                    <td>@{{inv.member_code}}</td>
                                    <td>@{{inv.name}}</td>
                                    <td>@{{inv.rate_group}}</td>
                                    <td>@{{formatMoney(CalRation(inv.member_fund))}}%</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <!-- My Rank -->

            </div>

            <div class="col-xl-3">

                <div class="news-section">

                    <div class="card">
                        <img class="card-img-top" src="assets/img/news-1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><strong>การฝากเงิน</strong></h5>
                            <p class="card-text">คุณสามารถทำการฝากเงินเพื่อคัดลอกการเทรด ได้ที่ปุ่มฝากเงิน
                                ใต้ข้อมูลสรุปผลการลงทุนของคุณ</p>
                            <strong>และรอการยืนยันรายการคำสั่งจาก Admin</strong>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top" src="assets/img/news-2.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><strong>การถอนเงิน</strong></h5>
                            <p class="card-text">คุณสามารถถอนเงินได้จากปุ่มถอนเงิน ซึ่งถอนได้ทั้งหมด 3 แบบ คือ
                                ถอนแล้วโอนเป็นทุน, ถอนผลตอบแทนและถอนทุน</p>
                            <strong>และรอการยืนยันรายการคำสั่งจาก Admin</strong>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="lds-spinner" v-show="loadPage == true">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<!-- appmodalDeposit -->
<div id="appmodalDeposit" v-cloak>
    <!-- use the modal component, pass in the prop -->
    <modal v-if="showModal" color="#17a2b8">
        <!--
                you can use custom content here to overwrite
                default content
            -->
        <div slot="header" class="d-flex justify-content-between align-items-center w-100">
            <h3>Deposit</h3>
        </div>
        <div slot="body">
            <form @submit.prevent="AddFund" enctype="multipart/form-data">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            ฝากเงินเข้าบัญชี โดยการโอนมาที่ 9802007490 ธนาคารกสิกรไทย ชื่อบัญชี นายสุรสิทธิ์ คำยันต์
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="col-form-label">Slip :</label>
                                <input type="file" accept="image/*" class="form-control" ref="Slip"
                                    v-on:change="onFileChange">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="col-form-label">Amount :</label>
                                <input type="number" class="form-control" v-model.number="fund" placeholder="0">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-md-end">
                        <div class="col-12 col-md-6 d-flex">
                            <button type="reset" class="btn btn-sm btn-default flex-grow-1 mr-2"
                                v-on:click.stop.prevent="CloseModal()">
                                Close
                            </button>
                            <button type="submit" class="btn btn btn-sm btn-success flex-grow-1"
                                v-bind:disabled="Waiting">
                                <span class="inline-block">+ Add</span>
                                <i v-show="Waiting" class="fal fa-spinner-third spin-ani"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </modal>
</div>
</div>
<!-- appmodalWithDraw -->
<div id="appmodalWithDraw" v-cloak>
    <!-- use the modal component, pass in the prop -->
    <modal v-if="showModal" color="#cb171e">
        <!--
                you can use custom content here to overwrite
                default content
            -->
        <div slot="header" class="d-flex justify-content-between align-items-center w-100">
            <h3>WithDraw</h3>
        </div>

        <div slot="body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">Type :</label>
                            <select class="form-control" v-model.number="type">
                                <option value="1">แปลงเป็นทุน</option>
                                <option value="2">ถอนผลตอบแทน</option>
                                <option value="3">ถอนทุน</option>
                                <option value="4">ถอนคอมมิสชั่น</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">Amount :</label>
                            <input type="number" class="form-control" v-model.number="withdraw" placeholder="0"
                                v-on:keyup.enter="WDconfirm()">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-end">
                    <div class="col-12 col-md-6 d-flex">
                        <button class="btn btn-sm btn-default flex-grow-1 mr-2" v-on:click.stop.prevent="CloseModal()">
                            Close
                        </button>
                        <button class="btn btn-sm btn-primary flex-grow-1" v-bind:disabled="Waiting"
                            v-on:click="WDconfirm()">
                            <span class="inline-block">Confirm</span>
                            <i v-show="Waiting" class="fal fa-spinner-third spin-ani"></i>

                        </button>
                    </div>
                </div>
            </div>
        </div>
    </modal>
</div>
</div>
<!-- Nav data bottom -->
<div id="navFooter" class="navMobile d-flex d-lg-none" :class="{open: showNav}">
    <div class="d-flex flex-wrap" v-cloak>
        <div class="w-100 d-flex box-icon box-icon--1">
            <div class="wrap-icon">
                <i class="fal fa-redo-alt "></i>
                <small class="">ผลตอบแทน</small>
            </div>
            <h6 class="text-success">
                @{{formatMoney(allreturn)}} บาท
            </h6>
        </div>
        <div class="w-100 d-flex box-icon box-icon--3">
            <div class="wrap-icon">
                <i class="fal fa-usd-circle"></i>
                <small class="">
                    รวมทั้งหมด
                </small>
            </div>


            <h6>
                @{{formatMoney(total)}} บาท
            </h6>
        </div>

        <div class="w-100 d-flex box-icon box-icon--2">
            <div class="wrap-icon">
                <i class="fas fa-usd-circle"></i>
                <small class="">ทุนเริ่มต้น</small>
            </div>

            <h6>
                @{{formatMoney(fund)}} บาท
            </h6>
        </div>
        <div class="w-100 d-flex box-icon box-icon--4">

            <div class="wrap-icon">
                <i class="fal fal fa-users"></i>
                <small class="">
                    คอมมิสชั่น
                </small>
            </div>
            <h6>
                @{{formatMoney(allcom)}} บาท
            </h6>
        </div>

    </div>
</div>
<!--script-->
<script type="text/x-template" id="modal-template">
    <transition name="modal">
            <div class="modal-mask">
                <div class="modal-wrapper" @click="$emit('close')">
                <div class="modal-container">

                    <div class="modal-header" v-bind:style="{background:color}">
                    <slot name="header">
                        default header
                    </slot>
                    </div>
                    <div class="modal-body">
                    <slot name="body">
                        default body
                    </slot>
                    </div>
                    <!--
                    <div class="modal-footer">
                    <slot name="footer">
                        default footer
                    </slot>
                    </div>-->
                </div>
                </div>
            </div>
            </transition>
        </script>
<script src="{{asset('/js/user-dashboard.js?16')}}"></script>
@endsection
