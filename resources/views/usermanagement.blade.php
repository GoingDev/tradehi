@extends('layouts.layout')

@section('content')
<style>
    h3 {
        margin-top: 10px;
        margin-bottom: 20px;
    }
    td,th {
        vertical-align: middle !important;
        text-align: center;
    }

    td>img {
        width: 70px;
        height: 70px;
        transition: transform .2s;
        margin: 0 auto;
        cursor: zoom-in;
    }
    .img_profile {
        max-width:120px;
    }
    td>img:hover {
        -ms-transform: scale(7);
        /* IE 9 */
        -webkit-transform: scale(7);
        /* Safari 3-8 */
        transform: scale(7);
        transition-delay:0.3s;
    }
    .wrapbtn-edit {
        text-align: right;
        margin-top: 10px;
    }
    [v-cloak] {
        display: none!important;
    }
</style>

<div class="content-area -admin" id="app">
    <!-- newuser -->
        <div class="row" v-cloak v-show="NoneUser == false">
            <!--table-->
                <div class="col-xl-12" v-if=" !Editshow ">
                    <h3>Waiting Verified</h3>
                    <table class="table table-light">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Passport</th>
                                <th>Bookbank</th>
                                <th>Status</th>
                                <th>Other</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="( row,index ) in datarow">
                                <td>@{{(index+1)}}</td>
                                <td>@{{row.email}}</td>
                                <td>@{{row.name}}</td>
                                <td><button class="btn btn-sm btn btn-default" v-on:click="view_img(index,'passport')">Passport</button></td>
                                <td><button class="btn btn-sm btn btn-default" v-on:click="view_img(index,'bookbank')">Bookbank</button></td>
                                <td><label style="color:#eea236;">Pending</label></td>
                                <td>
                                    <button class="btn btn-sm btn btn-default" v-on:click="Edituser(index)">Approvement</button>
                                    <button class="btn btn-sm btn btn-danger" v-on:click="Deluser(index)">Del</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            <!--Edit-->
                <div class="row col-xl-6 offset-3" v-if=" Editshow " v-cloak>
                    <h3>Approvement  @{{DataEdit.name}}</h3>
                    <div class="row form-group col-xl-12">
                        <label class="col-sm-3 col-form-label">Email :</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" v-model="DataEdit.email">
                        </div>
                    </div>
                    <div class="row form-group col-xl-12">
                        <label class="col-sm-3 col-form-label">Name :</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" v-model="DataEdit.name">
                        </div>
                    </div>
                    <div class="row form-group col-xl-12">
                        <label class="col-sm-3 col-form-label">ID/Passport :</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" placeholder="Passport ID" v-model.number="DataEdit.passport">
                        </div>
                    </div>
                    <div class="row form-group col-xl-12">
                        <label class="col-sm-3 col-form-label">Bookbank :</label>
                        <div class="col-sm-3">
                            <select class="form-control" v-model="DataEdit.bookbank_name">
                                <option value="BBL : ธนาคารกรุงเทพ จำกัด">BBL : ธนาคารกรุงเทพ จำกัด</option>
                                <option value="BBC : ธนาคารกรุงเทพพาณิชย์การ">BBC : ธนาคารกรุงเทพพาณิชย์การ</option>
                                <option value="KTB : ธนาคารกรุงไทย">KTB : ธนาคารกรุงไทย</option>
                                <option value="BAY : ธนาคารกรุงศรีอยุธยา">BAY : ธนาคารกรุงศรีอยุธยา</option>
                                <option value="KBANK : ธนาคารกสิกรไทย">KBANK : ธนาคารกสิกรไทย</option>
                                <option value="KBANK : ธนาคารกสิกรไทย">CITI : ธนาคารซิตี้แบงค์</option>
                                <option value="TMB : ธนาคารทหารไทย">TMB : ธนาคารทหารไทย</option>
                                <option value="SCB : ธนาคารไทยพาณิชย์">SCB : ธนาคารไทยพาณิชย์</option>
                                <option value="NBANK : ธนาคารธนชาติ">NBANK : ธนาคารธนชาติ</option>
                                <option value="SCIB : ธนาคารนครหลวงไทย">SCIB : ธนาคารนครหลวงไทย</option>
                                <option value="GSB : ธนาคารออมสิน">GSB : ธนาคารออมสิน</option>
                                <option value="GHB : ธนาคารอาคารสงเคราะห์">GHB : ธนาคารอาคารสงเคราะห์</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" placeholder="Bookbank number" v-model.number="DataEdit.bookbank_num">
                        </div>
                    </div>
                    <div class="row form-group col-xl-12">
                        <label class="col-sm-3 col-form-label">Status :</label>
                        <div class="col-sm-9">
                            <select class="form-control" v-model="DataEdit.status">
                                <option value="2">Pending</option>
                                <option value="3">Approve</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-12 wrapbtn-edit">
                        <button class="btn btn-default btn btn-sm" v-on:click="ClearformEdit()">Back</button>&nbsp;&nbsp;
                        <button class="btn btn-danger btn btn-sm" v-on:click="Editclick()">Submit</button>
                    </div>
                </div>
        </div>
    <!-- member -->
        <div class="row" v-if="loaddatamember == false" v-cloak>
            <!--table-->
                <div class="col-xl-12" v-if=" !Editmembershow ">
                    <div class="d-flex justify-content-between align-items-center">
                        <h3>Member Verified</h3>
                        <div v-show="datamemberLoader" class="mr-2">
                                <i class="fal fa-spinner-third spin-ani mr-2"></i>
                            </div>
                    </div>

                    <div class="table-responsive">
                            <table class="table table-light">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Email</th>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Fund</th>
                                            <th>ID/Passport</th>
                                            <th>Status</th>
                                            <th>Other</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(member,index) in datamember">
                                            <td>@{{(index+1)}}</td>
                                            <td>@{{member.email}}</td>
                                            <td>@{{member.name}}</td>
                                            <td>@{{member.member_code}}</td>
                                            <td>@{{member.member_fund}} THB</td>
                                            <td>@{{member.passport}}</td>
                                            <td><label style="color:green;">Approved</label></td>
                                            <td>
                                                <a :href="'/viewinvestor/'+member.u_id"><i class="fas fa-eye"></i></a>&nbsp;&nbsp;
                                                <button class="btn btn-defualt btn btn-sm" v-on:click="Editmember(index)">Edit</button>
                                                <!--<button class="btn btn-danger btn btn-sm" v-on:click="Delmember(index)">Del</button>--></td>
                                        </tr>
                                    </tbody>
                                </table>
                    </div>

                </div>
            <!--Edit-->
                <div class="row col-xl-8 offset-2" v-if=" Editmembershow " v-cloak>
                    <div class="row col-xl-12">
                        <h3>Edit Profile @{{DataEditMember.name}}</h3>
                    </div>
                    <form v-on:submit="formSubmit" enctype="multipart/form-data">
                    <div class="row col-xl-12">
                        <div class="col-md-3 text-center">
                            <img class="img_profile" v-bind:src="DataEditMember.img" style="border-radius:5px;" onclick="mainmodal_file.click()">
                            <input type="file" accept="image/*" name="main_file" id="mainmodal_file" style="display:none" v-on:change="onFileChange">
                            <br>
                            <a href="javascript:;" style="color:#dc3545;" onclick="mainmodal_file.click()">
                                <small >*Click to upload</small>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <div class="row form-group col-xl-12">
                                <label class="col-sm-3 col-form-label">Email :</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" v-model.number="DataEditMember.email">
                                </div>
                            </div>
                            <div class="row form-group col-xl-12">
                                <label class="col-sm-3 col-form-label">Name :</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" v-model.number="DataEditMember.name">
                                </div>
                            </div>
                            <div class="row form-group col-xl-12">
                                <label class="col-sm-3 col-form-label">ID/Passport :</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" v-model.number="DataEditMember.passport">
                                </div>
                            </div>
                            <div class="row form-group col-xl-12">
                                <label class="col-sm-3 col-form-label">Bookbank :</label>
                                <div class="col-sm-3">
                                    <select class="form-control" v-model="DataEditMember.bookbank_name">
                                        <option value="BBL : ธนาคารกรุงเทพ จำกัด">BBL : ธนาคารกรุงเทพ จำกัด</option>
                                        <option value="BBC : ธนาคารกรุงเทพพาณิชย์การ">BBC : ธนาคารกรุงเทพพาณิชย์การ</option>
                                        <option value="KTB : ธนาคารกรุงไทย">KTB : ธนาคารกรุงไทย</option>
                                        <option value="BAY : ธนาคารกรุงศรีอยุธยา">BAY : ธนาคารกรุงศรีอยุธยา</option>
                                        <option value="KBANK : ธนาคารกสิกรไทย">KBANK : ธนาคารกสิกรไทย</option>
                                        <option value="KBANK : ธนาคารกสิกรไทย">CITI : ธนาคารซิตี้แบงค์</option>
                                        <option value="TMB : ธนาคารทหารไทย">TMB : ธนาคารทหารไทย</option>
                                        <option value="SCB : ธนาคารไทยพาณิชย์">SCB : ธนาคารไทยพาณิชย์</option>
                                        <option value="NBANK : ธนาคารธนชาติ">NBANK : ธนาคารธนชาติ</option>
                                        <option value="SCIB : ธนาคารนครหลวงไทย">SCIB : ธนาคารนครหลวงไทย</option>
                                        <option value="GSB : ธนาคารออมสิน">GSB : ธนาคารออมสิน</option>
                                        <option value="GHB : ธนาคารอาคารสงเคราะห์">GHB : ธนาคารอาคารสงเคราะห์</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" v-model.number="DataEditMember.bookbank_num">
                                </div>
                            </div>
                            <div class="row form-group col-xl-12">
                                <label class="col-sm-3 col-form-label">Address :</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" v-model.number="DataEditMember.address"></textarea>
                                </div>
                            </div>
                            <div class="row form-group col-xl-12">
                                <label class="col-sm-3 col-form-label">Tel :</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" v-model.number="DataEditMember.tel">
                                </div>
                            </div>
                            <div class="row form-group col-xl-12">
                                <label class="col-sm-3 col-form-label">Fund :</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" v-model.number="DataEditMember.fund" placeholder="THB">
                                </div>
                            </div>
                            <div class="col-xl-12 wrapbtn-edit">
                                <button class="btn btn-default btn btn-sm" v-on:click.stop.prevent=" Editmembershow=false; ">Back</button>&nbsp;&nbsp;
                                <button class="btn btn-danger btn btn-sm">Submit</button>
                            </div>
                            </div>
                    </div>
                    </form>
                </div>
        </div>
</div>

<!-- appmodal -->
    <div id="appmodal" v-cloak>
        <!-- use the modal component, pass in the prop -->
        <modal v-if="showModal" @close="showModal = false">
            <!--
                you can use custom content here to overwrite
                default content
            -->
            <h3 slot="header">@{{header}}</h3>
            <h3 slot="body"><img class="img_modal" :src="img_modal"></h3>
        </modal>
    </div>
    </div>

<!--script-->
    <script type="text/x-template" id="modal-template">
        <transition name="modal">
            <div class="modal-mask">
                <div class="modal-wrapper" @click="$emit('close')">
                <div class="modal-container">

                    <div class="modal-header">
                    <slot name="header">
                        default header
                    </slot>
                    </div>

                    <div class="modal-body">
                    <slot name="body">
                        default body
                    </slot>
                    </div>

                    <div class="modal-footer">
                    <slot name="footer">
                        <button class="btn btn-primary btn btn-sm modal-default-button" @click="$emit('close')">
                        Close
                        </button>
                    </slot>
                    </div>
                </div>
                </div>
            </div>
            </transition>
        </script>
    <script src="{{asset('/js/usermanagement.js')}}"></script>
@endsection
