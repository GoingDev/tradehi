@extends('layouts.layout')

@section('content')
<style>
    .table-partner img {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        margin-right: 10px;
    }

    .padd2 {
        background-color: #ededed;

    }

    .padd2>td:nth-child(1) {
        padding-left: 4rem !important;
    }

    .padd4 {
        background-color: #e1e1e1;
    }

    .padd4>td:nth-child(1) {
        padding-left: 8rem !important;
    }

    i {
        cursor: pointer;
    }

    td {
        vertical-align: middle !important;
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        border-color: #b3b3b3 !important;
    }

</style>
<div class="content-area" id="partners">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-users"></i>&nbsp;Partners</h3>
                    <div v-show="partnersLoader"  class="ml-auto">
                        <div>
                            <i class="fal fa-spinner-third spin-ani mr-2"></i>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive table-partner">
                    <table class="table table-sm" >
                        <thead>
                            <th>#</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Profit</th>
                            <th>Commission</th>
                        </thead>
                        <tbody>
                            <tr v-cloak>
                                <td style="padding-left: 1rem!important;">
                                    <img :src="'/picture/'+myData.member_img" v-if="myData.member_img != null">
                                    <img src="/picture/avatar.jpg" v-else>

                                </td>
                                <td>
                                    <p>@{{myData.name}}</p>
                                </td>
                                <td>@{{myData.member_code}}</td>
                                <td>@{{formatMoney(myData.member_profit)}} baht</td>
                                <td>@{{formatMoney(myData.member_commission)}} baht</td>
                            </tr>
                            <tr  v-cloak v-for="x in row" v-show="x.show" :class="x.cl">
                                <td class="d-flex align-items-center">
                                    <i class="fa fa-plus-circle mr-1" v-on:click="ShowChild(x.id)" v-if=" x.cl=='padd2' "></i>
                                    <img :src="'/picture/'+x.img" v-if="x.img != null">
                                    <img src="/picture/avatar.jpg" v-else>

                                </td>
                                <td>
                                    <p>@{{x.name}}</p>
                                </td>
                                <td>@{{x.code}}</td>
                                <td>@{{formatMoney(x.profit)}} baht</td>
                                <td>@{{formatMoney(x.com)}} baht</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="{{asset('/js/partners.js')}}"></script>
@endsection
