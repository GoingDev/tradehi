@extends('layouts.layout')

@section('content')
<div class="content-area -admin" id="app">
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10 offset-xl-1">
            <div class="d-flex justify-content-between align-items-center">
                    <h3>Class Management</h3>
                    <div v-show="dataGroupRowLoader" class="mr-2">
                            <i class="fal fa-spinner-third spin-ani mr-2"></i>
                        </div>
            </div>
                
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Class name</td>
                                <td>Profit return (%)</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-cloak v-for="( row ) in dataGroupRow">
                                <td>@{{row.rate_group}} </td>
                                <td>@{{row.rate_return}}</td>
                                <td>
                                    <a href="#" class="btn btn-warning btn-sm" v-on:click="EditRate(row.rate_id, row.rate_group, row.rate_return)"><i class="fas fa-edit"></i></a>
                                    <a href="#" v-if='row.rate_id!=1' class="btn btn-danger btn-sm" v-on:click="DelRate(row.rate_id)"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" class="form-control" v-model="groupRateDate.group"/>
                                </td>
                                <td>
                                    <input type="number" class="form-control" max="100" v-model.number="groupRateDate.rate"/>
                                </td>
                                <td>
                                    <a href="#" v-if="isEdit" class="btn btn btn-sm" v-on:click="Clear()"><i class="fas fa-times"></i></a>
                                    <a href="#" v-if="isEdit" class="btn btn-success btn-sm" v-on:click="UpdateGroupRate()"><i class="fas fa-check"></i></a>
                                    <a href="#" v-else class="btn btn-success btn-sm" v-on:click="AddGroupRate()"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
        </div>
       
    </div>
</div>

<script src="{{asset('/js/setting.js')}}"></script>
@endsection
