<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="icon" href="{{asset('logo.ico')}}">
    <title>TradeHi</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
    <section class="welcome-home">
        <div class="links">
            @auth
            <a href="{{ url('/home') }}">Dashboard</a>
            @else
            <a href="{{ route('login') }}">Login</a>
            <a href="{{ route('register') }}">Register</a>
            @endauth
        </div>
        <div class="member-login -register">
            <a href="/" class="main-logo">
                <img src="/assets/img/logo-hor.png" alt="">
            </a>

            <p>Register as an investor with TradeHi today, receive many benefits and privileges.</p>

            <form class="login-form register-form" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
                @csrf
                <div class="row form-group align-items-center">
                    <div class="col-sm-3"><label for="exampleInputEmail1"><i class="fal fa-envelope"></i> Email address</label></div>
                    <div class="col-sm-9"><input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                            name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                            else.</small>
                    </div>
                </div>
                <div class="row form-group align-items-center">
                    <div class="col-sm-3"><label for="exampleInputEmail1"><i class="fal fa-key"></i> Password</label></div>
                    <div class="col-sm-3"><input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                            name="password" required>

                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif</div>

                    <div class="col-sm-3"><label for="exampleInputEmail1"><i class="fal fa-key"></i> Re-Password</label></div>
                    <div class="col-sm-3"><input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required></div>
                </div>
                <hr>
                <div class="row form-group align-items-center">
                    <div class="col-sm-3"><label for="exampleInputEmail1"><i class="fal fa-user"></i> Full Name</label></div>
                    <div class="col-sm-9"><input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                            name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif</div>
                </div>

                <hr>
                <div class="my-4">
                    <strong class="h4">Please Upload Document</strong>
                </div>

                <p>Register as an investor with TradeHi today, receive many benefits and privileges.</p>
                <div class="row form-group">
                    <div class="col-md-6">
                        <p class="h5">ID/Passport</p>
                        <input  accept="image/*" type="file" class="form-control" name="passport_img" required>
                    </div>
                    <div class="col-md-6">
                        <p class="h5">Bookbank</p>
                        <input  accept="image/*" type="file" class="form-control" name="bookbank_img" required>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        <input id="passport" type="text" class="form-control{{ $errors->has('passport') ? ' is-invalid' : '' }}"
                            name="passport" value="{{ old('passport') }}" required autofocus placeholder="ID/Passport">

                        @if ($errors->has('passport'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('passport') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" name="bank_name">
                            <option value="BBL : ธนาคารกรุงเทพ จำกัด">BBL : ธนาคารกรุงเทพ จำกัด</option>
                            <option value="BBC : ธนาคารกรุงเทพพาณิชย์การ">BBC : ธนาคารกรุงเทพพาณิชย์การ</option>
                            <option value="KTB : ธนาคารกรุงไทย">KTB : ธนาคารกรุงไทย</option>
                            <option value="BAY : ธนาคารกรุงศรีอยุธยา">BAY : ธนาคารกรุงศรีอยุธยา</option>
                            <option value="KBANK : ธนาคารกสิกรไทย">KBANK : ธนาคารกสิกรไทย</option>
                            <option value="KBANK : ธนาคารกสิกรไทย">CITI : ธนาคารซิตี้แบงค์</option>
                            <option value="TMB : ธนาคารทหารไทย">TMB : ธนาคารทหารไทย</option>
                            <option value="SCB : ธนาคารไทยพาณิชย์">SCB : ธนาคารไทยพาณิชย์</option>
                            <option value="NBANK : ธนาคารธนชาติ">NBANK : ธนาคารธนชาติ</option>
                            <option value="SCIB : ธนาคารนครหลวงไทย">SCIB : ธนาคารนครหลวงไทย</option>
                            <option value="GSB : ธนาคารออมสิน">GSB : ธนาคารออมสิน</option>
                            <option value="GHB : ธนาคารอาคารสงเคราะห์">GHB : ธนาคารอาคารสงเคราะห์</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="bank_num" required placeholder="Bookbank Number">
                    </div>
                </div>
                @if(isset($invite_id))
                    <input name="invite_id" value="{{$invite_id}}" hidden>
                @else
                    <input name="invite_id" value="3" hidden>
                @endif
                <button type="submit" class="btn btn-primary btn-block"  >
                    Submit
                </button>
            </form>
        </div>
    </section>
</body>

</html>
