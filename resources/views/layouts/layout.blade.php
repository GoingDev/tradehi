<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('logo.ico')}}">
    <title>TradeHi</title>
    @php
        $url = substr(Request::url(),7,9);
    @endphp
    @if($url == "localhost")
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
    @else
        <script src="{{asset('/assets/js/vue.min.js')}}"></script>
    @endif

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.all.min.js"></script>
    <script src="{{asset('/assets/js/lodash.min.js')}}"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-database.js"></script>
    <script src="{{asset('/js/firebaseConnection.js?1')}}"></script>
    <script src="{{asset('/assets/js/clipboard.min.js')}}"></script>
    <script src="{{asset('/assets/js/moment.js')}}"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    {{-- <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-POYwD7xcktv3gUeZO5s/9nUbRJG/WOmV6jfEGikMJu77LGYO8Rfs2X7URG822aum" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="{{ asset('css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
    <!-- Nav Bar -->
    @include('layouts.nav')
    @if(Auth::user()->status != "2")
    <!-- Main Section -->
    <section class="main-section">
        @include('layouts.aside')
        <article class="main-content">
            @include('layouts.myProfile')
            @yield('content')
        </article>
    </section>
    @else
        <div class="row justify-content-center mt-60">
            <div class="col-md-12">
                <div class="box-verified">
                    Your account is currently being verified.
                </div>
            </div>
        </div>
    @endif


    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>

            $(".Has-sub").click(function(event){
                $(this).children('ul').toggle('active');
            });
    </script>

</body>

</html>
