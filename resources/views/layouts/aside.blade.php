<style>
    .sub{
        display: none;
    }
    .active{
        display: block;
    }
    li {
        cursor: pointer;
    }
</style>
<aside class="side-bar d-none d-xl-block">
    <ul class="side-menu">
        <li style="margin-left: -8px; margin-bottom: 15px; font-size: 14px;cursor:pointer;">{{Auth::user()->name}}</li>
        <li><a href="{{url('/dashboard')}}"><i class="fal fa-tachometer"></i> Dashboard</a></li>
        <li><a href="{{url('/EditProfile')}}"><i class="fal fa-user"></i> Profile</a></li>
        <li><a href="{{url('/Partners')}}"><i class="fal fa-users"></i> Partners</a></li>
        <li><a href="{{url('/History')}}"><i class="fal fa-usd-circle"></i>History</a></li>
        <li><a href="{{url('/Profit')}}"><i class="fal fa-chart-line"></i> Profit</a></li>
        <li><a href="{{url('/Contact')}}"><i class="fal fa-envelope"></i> Contact</a></li>

        @if(Auth::user()->status == 1)
        <li class="Has-sub">
            <a><i class="fal fa-cog"></i>Admin System</a>
            <ul class="sub">
                <li><a href="{{url('/admin')}}"> Dashboard</a></li>
                <li><a href="{{url('/usermanagement')}}">User Manage</a></li>
                <li><a href="{{url('/setting')}}">Settings</a></li>
            </ul>
        </li>

        @endif
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fal fa-sign-out-alt"></i>   {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</aside>
