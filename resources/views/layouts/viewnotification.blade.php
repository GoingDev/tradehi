<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('logo.ico')}}">
    <title>TradeHi</title>
    @php
    $url = substr(Request::url(),7,9);
    @endphp
    @if($url == "localhost")
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
    @else
    <script src="{{asset('/assets/js/vue.min.js')}}"></script>
    @endif
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.all.min.js"></script>
    <script src="{{asset('/assets/js/lodash.min.js')}}"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-database.js"></script>
    <script src="{{asset('/js/firebaseConnection.js')}}"></script>
    <script src="{{asset('/assets/js/moment.js')}}"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    {{--
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-POYwD7xcktv3gUeZO5s/9nUbRJG/WOmV6jfEGikMJu77LGYO8Rfs2X7URG822aum"
        crossorigin="anonymous"> --}}
        <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>

<body>
    <!-- Nav Bar -->
    @include('layouts.nav')
    @if(Auth::user()->status != "2")
    <!-- Main Section -->
    
    <div class="mt-60 container">
        <div class="row justify-content-center">
            <div class="col-md-10" id="Noticbody">
                <h5 class="mt-4">การแจ้งเตือนของคุณ</h5>
                <hr>
                <ul class="drop-menu drop-menu-noti-all" v-cloak>
                    <li v-for="x in noticData" :class="{'bg-white': (x.status == 2)}">
                        <div v-on:click="gotoLink(x.link,x)" class="d-flex align-items-center">
                            <div>
                                <i class="fas fa-user-plus" v-if="x.type==0"></i><!-- register -->
                                <i class="fas fa-usd-circle" v-else-if="x.type==1"></i><!-- user deposit -->
                                <i class="fas fa-hand-holding-usd" v-else-if="x.type==2"></i><!-- user withdraw -->
                                <i class="fas fa-hand-holding-usd" v-else-if="x.type==3" style="color:green;"></i><!-- approve Withdraw -->
                                <i class="fas fa-usd-circle" v-else-if="x.type==4" style="color:green;"></i><!-- approve Depoist -->
                                <i class="fab fa-bitcoin" v-else-if="x.type==5"></i> <!-- profit -->
                                <i class="fas fa-hand-holding-usd" v-else-if="x.type==6" style="color:red;"></i><!-- Reject Withdraw -->
                                <i class="fas fa-usd-circle" v-else-if="x.type==7" style="color:red;"></i><!-- Reject Deposit -->
                            </div>
                            <div class="des">
                                <h5>
                                    <strong>@{{x.name}}</strong>
                                </h5>
                                <h6>@{{x.action}}</h6>
                                <small>@{{x.created}}</small>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    @else
    <div class="container mt-60"> 
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="box-verified">
                    Your account is currently being verified.
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $(".Has-sub").click(function (event) {
            $(this).children('ul').toggle('active');
        });

    </script>
    <script>
        const User = new Vue({
            el: '#Noticbody',
            data: {
                id: '{{Auth::user()->id}}',
                noticData: []
            },
            methods: {
                noticBtnClick() {
                    this.notic_show = !this.notic_show
                    let data = this.noticData;
                    for (let i in data) {
                        if (data[i].status == 0) {
                            let updates = {
                                notic_action: data[i].action,
                                notic_created: data[i].created,
                                notic_link: data[i].link,
                                notic_name: data[i].name,
                                notic_seen: data[i].seen,
                                notic_status: 1,
                                notic_type: data[i].type,
                                notic_uid: data[i].u_id
                            }
                            firebase.database().ref('Notic/' + data[i].key).update(updates);
                        }
                    }
                },
                async gotoLink(link, data) {
                    let updates = await {
                        notic_action: data.action,
                        notic_created: data.created,
                        notic_link: data.link,
                        notic_name: data.name,
                        notic_seen: data.seen,
                        notic_status: 2,
                        notic_type: data.type,
                        notic_uid: data.u_id
                    }
                    let firebaseupdate = await firebase.database().ref('Notic/' + data.key).update(updates);
                    window.location.assign(link);
                }
            }

        });

    </script>
</body>

</html>
