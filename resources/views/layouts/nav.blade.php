<!-- Nav Bar -->

<div class="main-nav" id="nav">
    <nav v-cloak class="navbar navbar-expand-xl navbar-light bg-light">
        <a class="main-logo navbar-brand" href="{{url('/')}}">
            <img src="/assets/img/logo-hor.png" alt="">
        </a>

        <li class="d-block d-xl-none ml-auto mr-2" style="position:relative">
            <a class="nav-link noticBtn" v-on:click="noticBtnMobileClick()"><i class="fas fa-bell"></i>
                <label class="wrap-text-Notic" v-show="show_count">@{{notic_count}}</label>
            </a>
            <ul class="drop-menu drop-menu-mobile" v-show="notic_show_mobile">
                <li v-for="x in noticData.slice(0,5)" :class="{'bg-white': (x.status == 2)}">
                    <div v-on:click="gotoLink(x.link,x)" class="d-flex align-items-center">
                        <div>
                            <i class="fas fa-user-plus" v-if="x.type==0"></i><!-- register -->
                            <i class="fas fa-usd-circle" v-else-if="x.type==1"></i><!-- user deposit -->
                            <i class="fas fa-hand-holding-usd" v-else-if="x.type==2"></i><!-- user withdraw -->
                            <i class="fas fa-hand-holding-usd" v-else-if="x.type==3" style="color:green;"></i><!-- approve Withdraw -->
                            <i class="fas fa-usd-circle" v-else-if="x.type==4" style="color:green;"></i><!-- approve Depoist -->
                            <i class="fab fa-bitcoin" v-else-if="x.type==5"></i> <!-- profit -->
                            <i class="fas fa-hand-holding-usd" v-else-if="x.type==6" style="color:red;"></i><!-- Reject Withdraw -->
                            <i class="fas fa-usd-circle" v-else-if="x.type==7" style="color:red;"></i><!-- Reject Deposit -->
                        </div>
                        <div class="des">
                            <h5>
                                <strong>@{{x.name}}</strong>
                            </h5>
                            <h6 class="text-success">@{{x.action}}</h6>
                            <small>@{{x.created}}</small>
                        </div>
                    </div>
                </li>
                <li class="text-center bg-white view-all">
                    <a href="/notic"> ดูทั้งหมด</a>

                </li>
            </ul>
        </li>

        <button v-on:click="noticBtnMobileClickClose()" class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <i class="fal fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Authentication Links -->
            <form class="d-lg-none form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">

            </form>
            <ul class="navbar-nav mr-auto d-xl-none">
                @if(Auth::user()->status != "2")
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/dashboard')}}">Dashboard <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item"><a href="{{url('/EditProfile')}}" class="nav-link">Profile</a></li>
                <li class="nav-item"><a href="{{url('/Partners')}}" class="nav-link">Partners</a></li>
                <li class="nav-item"><a href="{{url('/History')}}" class="nav-link">History</a></li>
                <li class="nav-item"><a href="{{url('/Profit')}}" class="nav-link">Profit</a></li>
                <li class="nav-item"><a href="{{url('/Contact')}}" class="nav-link">Contact</a></li>
                @endif
                @if(Auth::user()->status == 1)
                <div class="dropdown-divider"></div>
                <div class="nav-link"><i class="fa fa-cog">Admin System</i></div>
                <li class="nav-item"><a href="{{url('/admin')}}" class="nav-link"> Dashboard</a></li>
                <li class="nav-item"><a href="{{url('/usermanagement')}}" class="nav-link">User Manage</a></li>
                <li class="nav-item"><a href="{{url('/setting')}}" class="nav-link">Settings</a></li>
                @endif
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fal fa-sign-out-alt"></i> {{ __('Logout') }}
                </a>
                <form id="logout-form-nav" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="d-none d-xl-block mr-2" style="position:relative;" v-click-outside="close">
                    <a class="nav-link noticBtn" v-on:click="noticBtnClick()"><i class="fas fa-bell"></i>
                        <label class="wrap-text-Notic" v-show="show_count">@{{notic_count}}</label>
                    </a>
                    <ul class="drop-menu" v-show="notic_show">
                        <li v-for="x in noticData.slice(0,5)" :class="{'bg-white': (x.status == 2)}">
                            <div v-on:click="gotoLink(x.link,x)" class="d-flex align-items-center">
                                <div>
                                    <i class="fas fa-user-plus" v-if="x.type==0"></i><!-- register -->
                                    <i class="fas fa-usd-circle" v-else-if="x.type==1"></i><!-- user deposit -->
                                    <i class="fas fa-hand-holding-usd" v-else-if="x.type==2"></i><!-- user withdraw -->
                                    <i class="fas fa-hand-holding-usd" v-else-if="x.type==3" style="color:green;"></i><!-- approve Withdraw -->
                                    <i class="fas fa-usd-circle" v-else-if="x.type==4" style="color:green;"></i><!-- approve Depoist -->
                                    <i class="fab fa-bitcoin" v-else-if="x.type==5"></i> <!-- profit -->
                                    <i class="fas fa-hand-holding-usd" v-else-if="x.type==6" style="color:red;"></i><!-- Reject Withdraw -->
                                    <i class="fas fa-usd-circle" v-else-if="x.type==7" style="color:red;"></i><!-- Reject Deposit -->
                                </div>
                                <div class="des">
                                    <h5>
                                        <strong>@{{x.name}}</strong>
                                    </h5>
                                    <h6>@{{x.action}}</h6>
                                    <small>@{{x.created}}</small>
                                </div>
                            </div>
                        </li>
                        <li class="text-center bg-white view-all">
                            <a href="/notic"> ดูทั้งหมด</a>
                        </li>
                    </ul>
                </li>
                <!-- Authentication Links -->
                <form class="d-none d-xl-block form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </ul>
        </div>
    </nav>
</div>


<script src="{{asset('/js/nav.js')}}"></script>
