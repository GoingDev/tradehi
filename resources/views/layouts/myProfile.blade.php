<!-- My Profile -->
<style>
    [v-cloak] {
        display: none!important;
    }
    .linkregisterbtn{
        position: relative;
        z-index : 3;
        display: inline-block;
        opacity: 1;
        visibility: visible;
    }
    .btn-reload,
    .linkregisterbtn {
        background-color:rgb(221, 221, 221)!important;
    }
</style>
<div class="my-profile d-flex align-items-center" id="User">
    <ul class="list-unstyled">
        <li class="media">
            <img class="mr-3 avatar" :src="img" alt="">
            <div class="media-body">
                <h5 class="mt-0 mb-1 greeting">Hello! {{Auth::user()->name}}</h5>
                <p class="small" v-cloak>Class @{{classgroup}}</p>
                @if(Auth::user()->id != 1)
                <input  ref="Reflinkregister" id="linkregister" class="linkregister" type="text" name="" v-bind:value="linkregister">
                <button :data-clipboard-text="linkregister" :disabled="linkregister == '' ? true : false" v-on:click="CopytoClipboard" id="btn-linkregister" class="btn btn-default btn btn-sm linkregisterbtn"><i class="fal fa-clone"></i></button>
                
                @endif
            </div>
        </li>
    </ul>
</div>
<script>
    const User = new Vue({
        el : '#User',
        data : {
            img : '',
            id : '{{Auth::user()->id}}',
            classgroup : '',
            linkregister : '',
        },
        created() {
            const vm = this;
                axios.get('/api/getmember/'+this.id)
                .then(function(res){
                    let img = res.data[0].member_img;
                    let code = res.data[0].member_code;
                    vm.classgroup = res.data[0].rate_group;
                    if(img != null){
                        vm.img = "/picture/"+res.data[0].member_img;
                    }
                    else{
                        vm.img = "/picture/avatar.jpg";
                    }
                    vm.linkregister = 'https://tradehi.co/register/'+ code + "/" + vm.id;
                }).catch(function(res){
                    vm.img = "/picture/avatar.jpg";
                });
        },
        methods: {
            CopytoClipboard() {
                const vm = this;
                let btn = document.getElementById('btn-linkregister');
                let clipboard = new ClipboardJS(btn);
                clipboard.on('success', function(e) {
                    swal({
                    title : 'Copy Link แล้ว',
                    text : vm.linkregister,
                    type : 'success'
                    });
                });
                clipboard.on('error', function(e) {
                    swal({
                    title : 'Copy Link ล้มเหลว',
                    text : 'ล้มเหลว',
                    type : 'error'
                    });
                });
            }
        }
    });
</script>
