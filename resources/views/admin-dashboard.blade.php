@extends('layouts.layout')

@section('content')
<style>

    i {
        margin: 5px;
        cursor: pointer;
        font-size: 20px;
    }
    .table-transaction tr > td:nth-last-child(1) {
        width: 100%;
        display: inline-flex;
        justify-content: flex-end;
    }
    .btn{
        margin-top:10px;
        margin-left:5px;
    }
    .modal-body{
        margin-bottom: 0px;
    }
    .table{
        margin-bottom : 0px;
    }
    .table td, .table th {
        vertical-align: middle;
    }

    [v-cloak] {
        display: none;
    }
    .btn-group i:hover{
        font-weight: 500;
    }
    @media only screen and (min-width: 720px) {
        .mt-box1,.mt-box2{
            margin-bottom: 1.25rem;
        }
    }
    @media only screen and (max-width: 720px) {
        .mt-box1,.mt-box2,.mt-box3{
            margin-bottom: 1.25rem;
        }
    }
    .btn{
        margin-top:0px!important;
    }

</style>

<div class="content-area -admin" id="app">
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-7">
                    <!-- My Fund -->
                    <div class="my-fund card border-success">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h3 class="title">
                                <i class="fas fa-usd-circle"></i> TradeHi Balance
                            </h3>
                            <div v-show="allProfitLoader" class="ml-auto">
                                <i class="fal fa-spinner-third spin-ani mr-2"></i>
                            </div>
                        </div>
                        <div class="card-body fund-summary">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="d-flex">
                                        <i class="fa-2x fal fa-plus-circle"></i>
                                        <div>
                                            <p class="amount" v-cloak>
                                                @{{formatMoney(allprofit)}} Baht
                                            </p>
                                            <p class="small">
                                                Profit
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="d-flex">
                                        <i class="fa-2x fal fa-redo-alt"></i>
                                        <div>
                                            <p class="amount" v-cloak>
                                                @{{formatMoney(allreturn)}} Baht
                                            </p>
                                            <p class="small">
                                                Return
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="d-flex">
                                        <i class="fa-2x fal fa-chart-line"></i>
                                        <div>
                                            <p class="amount" v-cloak>
                                                @{{formatMoney(allbalance)}} Baht
                                            </p>
                                            <p class="small">
                                                Balance
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="d-flex">
                                        <i class="fa-2x fal fal fa-users"></i>
                                        <div>
                                            <p class="amount" v-cloak>
                                                @{{formatMoney(allcom)}} Baht
                                            </p>
                                            <p class="small">
                                                Commission
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- My Fund -->
                </div>
                <div class="col-xl-5">
                    <!-- My Fund -->
                    <div class="my-fund border-success card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h3 class="title">
                                <i class="fas fa-usd-circle"></i> TradeHi Fund
                            </h3>
                            <div v-show="allfundLoader" class="ml-auto">
                                <i class="fal fa-spinner-third spin-ani mr-2"></i>
                            </div>
                        </div>
                        <div class="card-body fund-summary">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="d-flex">
                                        <i class="fa-2x fal fa-usd-circle"></i>
                                        <div>
                                            <p class="amount" v-cloak>
                                                @{{formatMoney(total)}} Baht
                                            </p>
                                            <p class="small">
                                                Total
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="d-flex">
                                        <i class="fa-2x fas fa-usd-circle"></i>
                                        <div>
                                            <p class="amount" v-cloak>
                                                @{{formatMoney(allfund+allstag)}} Baht
                                            </p>
                                            <p class="small">
                                                Fund
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="d-flex">
                                        <i class="fa-2x fal fa-chart-line"></i>
                                        <div>
                                            <p class="amount" v-cloak>
                                                @{{grow}}%
                                            </p>
                                            <p class="small">
                                                Grow
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- My Fund -->

                </div>
            </div>
            <!-- My Investment -->
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-users"></i> TradeHi Investors</h3>
                    <div class="ml-auto d-flex">
                        <div v-show="investorLoader" class="mr-2">
                            <i class="fal fa-spinner-third spin-ani mr-2"></i>
                        </div>
                        <a href="{{ url('/investors_manage') }}" class="btn btn-sm btn-secondary">All Investors</a>
                    </div>
                </div>
                <div class="table-responsive">

                    <table class="table table-sm" v-cloak>
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Full Name</th>
                                <th scope="col">Fund</th>
                                <th scope="col">Class</th>
                                <th scope="col">Profit</th>
                                <th scope="col">Ratio</th>
                                <th scope="col">Return</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr v-for='(user,index) in investor.slice(0,4)'>
                                <th scope="row" class="member-id">
                                    @{{user.member_code}}
                                </th>
                                <td class="text-success">
                                    @{{user.name}}
                                </td>
                                <td>
                                    @{{formatMoney(user.member_fund)}} Baht
                                </td>
                                <td class="text-success">
                                    @{{user.rate_group}}
                                </td>
                                <td>
                                    @{{formatMoney(parseFloat(user.member_balance)+parseFloat(user.member_profit))}}
                                    Baht
                                </td>
                                <td>
                                    @{{formatMoney((parseFloat(user.member_fund)/parseFloat(allfund))*100)}} %
                                </td>
                                <td class="text-success">
                                    @{{formatMoney(user.member_profit)}} Baht
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a :href="'/viewinvestor/'+user.id"><i class="fal fa-eye"></i></a>
                                        <i class="fal fa-cog" v-on:click="EditRate(user.member_id,user.member_rate)"></i>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--
                <div class="card-footer text-right">
                    <a href="{{ url('/investors_manage') }}" class="btn btn-sm btn-secondary">All Investors</a>
                </div>-->
            </div>
            <!-- My Investment -->

        </div>
        <!-- Profit -->
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-chart-line"></i> TradeHi Profit</h3>
                    <div class="ml-auto d-flex">
                        <div v-show="profitLoader" class="mr-2">
                            <i class="fal fa-spinner-third spin-ani mr-2"></i>
                        </div>
                        <button v-on:click="NewProfit" class="btn btn-success btn-sm">Add New Profit</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-sm profit" v-cloak>
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Profit</th>
                                <th scope="col">Return</th>
                                <th scope="col">Balance</th>
                                <th scope="col">Commission</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="p in profit.slice(0,6)">
                                <th scope="row">@{{p.date}}</th>
                                <td>@{{p.time}}</td>
                                <td class="text-success">@{{formatMoney(p.amount)}} Baht</td>
                                <td>@{{formatMoney(p.return)}}</td>
                                <td>@{{formatMoney(p.balance)}}</td>
                                <td>@{{formatMoney(p.com)}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Profit -->
        </div>
        <!-- Transaction  -->
        <div class="col-xl-12">
            <!-- Transaction  -->
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-exchange"></i> Transaction Requests</h3>
                    <div class="ml-auto d-flex">
                        <div v-show="dataReqLoader" class="mr-2">
                            <i class="fal fa-spinner-third spin-ani mr-2"></i>
                        </div>
                        <a href="{{url('/viewallrequest')}}" class="btn btn-success btn-sm">View All</a>
                    </div>
                </div>
                <span class="table-responsive table-transaction">
                    <table class="table table-sm" v-cloak>
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Type</th>
                                <th scope="col">Investor</th>
                                <th scope="col">Bank</th>
                                <th scope="col">Status</th>
                                <th scope="col">Updated</th>
                                <th>Other</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(x,index) in dataReq.slice(0, 5)">
                                <th scope="row">@{{x.code}}</th>
                                <th>@{{x.date}}</th>
                                <td>@{{x.time}}</td>
                                <td class="text-success">@{{formatMoney(x.amount)}} Baht</td>
                                <td>@{{x.type}}</td>
                                <td><a :href="'/viewinvestor/'+x.u_id">@{{x.investor}}</a></td>
                                <td>
                                    <i class="fas fa-university" data-toggle="tooltip" :title="x.bank"></i>
                                </td>
                                <td>
                                    <span v-if="x.status == 1" style="color:orange;">Waiting</span>
                                    <span v-else-if="x.status == 2" style="color:green;">Approved</span>
                                    <span v-else-if="x.status == 3" style="color:red;">Rejected</span>
                                </td>
                                <td>@{{x.updated}}</td>
                                <td>
                                    <span v-if=" x.status == 1 ">
                                        <i v-if=" x.type == 'Deposit' " class="fas fa-image" v-on:click="View(index)"
                                            style="color:grey;"></i>
                                        <i class="fas fa-check" v-on:click="Approve(index,x.type)" style="color:grey;"></i>
                                        <i class="fas fa-times" v-on:click="Reject(index,x.type)" style="color:grey;"></i>
                                    </span>
                                    <span v-else-if=" x.status == 2 ">
                                        <i v-if=" x.type == 'Deposit' " class="fas fa-image" v-on:click="View(index)"
                                            style="color:grey;"></i>
                                        <i class="far fa-check-square" style="color:grey;"></i>
                                    </span>
                                    <span v-else-if=" x.status == 3 ">
                                        <i class="far fa-times-square" style="color:grey;"></i>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </div>

        </div>
        <!-- Transaction  -->
    </div>
</div>


<!-- appmodal -->
<div id="appmodal" v-cloak>
    <!-- use the modal component, pass in the prop -->
    <modal v-if="showModal" @close="showModal = false">
        <!--
                you can use custom content here to overwrite
                default content
            -->
        <h3 slot="header">Verify Img</h3>
        <h3 slot="body"><img class="img_modal" :src="img_modal"></h3>
    </modal>
</div>
<!-- appmodalProfit -->
<div id="appmodalProfit" v-cloak>
    <!-- use the modal component, pass in the prop -->
    <modal v-if="showModal">
        <!--
                you can use custom content here to overwrite
                default content
            -->
        <div slot="header" class="d-flex justify-content-between align-items-center w-100">
            <h3>Add New Profit</h3>
            <div v-show="Waiting">
                <div class="lds-spinner mini-spiner">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <div slot="body">
            <div class="row form-group col-xl-12">
                <label class="col-sm-3 col-form-label">Amount :</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" v-model.number="amount" v-on:keyup.enter="Profitconfirm()">
                </div>
            </div>
            <div class="btndeposit row form-group col-xl-12 d-flex justify-content-end">
                <button class="btn btn-default" v-on:click.stop.prevent="showModal=false">
                    Close
                </button>
                <button class="btn btn-success" v-bind:disabled="Waiting" v-on:click="Profitconfirm()">
                    Confirm
                </button>
            </div>
        </div>
    </modal>
</div>
{{-- rateEditor --}}
<div id="rateEditModal" v-cloak>
    <!-- use the modal component, pass in the prop -->
    <modal v-if="showModal">
        <h3 slot="header">Change group rate</h3>
        <div slot="body">
            <div class="row form-group col-xl-12">
                <select class="form-control" v-model="dataChange.rateSeleted">
                    <option v-for='item in dataGroupRow' :value="item.rate_id">@{{item.rate_group}}</option>
                </select>
            </div>
            <div class="btndeposit row form-group col-xl-12 d-flex justify-content-end">
                <button class="btn btn-default" v-on:click='showModal = false'>cancle</button>
                <button class="btn btn-success" v-on:click='UpdateMemberRate()'>save</button>
            </div>
        </div>
    </modal>
</div>
<!--script-->

<!--script-->
<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="$emit('close')">
            <div class="modal-container">

                <div class="modal-header">
                <slot name="header">
                    default header
                </slot>
                </div>

                <div class="modal-body">
                <slot name="body">
                    default body
                </slot>
                </div>
                <!--
                <div class="modal-footer">
                <slot name="footer">
                    default footer
                </slot>
                </div>-->
            </div>
            </div>
        </div>
        </transition>
    </script>
<script src="{{asset('/js/admin-dashboard.js?5')}}"></script>
@endsection
