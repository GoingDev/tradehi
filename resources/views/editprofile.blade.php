@extends('layouts.layout')
@section('content')
<style>
    .wrap-boxprofile{
        margin-top:40px;
    }
    .wrapbtn-edit{
        text-align: right;
        margin-top:40px;
        margin-bottom:40px;
    }
    .title{
        margin-bottom: 40px;
    }
    .img_profile {
        width: 128px;
        height: 128px;
        cursor: pointer;
    }
    form{
        width: 100%!important;
    }
    .alert{
        margin: auto;
        margin-bottom:20px;
    }
    [v-cloak] {
        display: none!important;
    }
</style>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="container-fluid">
    <div class="row justify-content-center wrap-boxprofile" id="app">
        <div v-if='isChangePassword' class="col-12 col-md-5 col-md-3">
            <h3 class="title text-center text-md-left">Change New Password</h3>

            <div class="form-group mt-4">
                <label class="col-form-label">Old Password :</label>
                <input type="password" class="form-control" v-model="password.oldPassword" />
                <span class="invalid-feedback" role="alert">
                    <strong>OhMom!</strong>
                </span>
            </div>
            <div class="form-group mt-4">
                <label class="col-form-label">New Password :</label>
                <input type="password" class="form-control" v-model="password.newPassword" />
            </div>
            <div class="form-group mt-4">
                <label class="col-form-label">Confirm Password :</label>
                <input type="password" class="form-control" v-model="password.confirmPassword" />
            </div>
            <div class="form-group mt-4 d-flex d-md-block">

                <button class="btn btn flex-grow-1 w-25 mr-2" v-on:click='changePasswordClear()'>Cancel</button>
                <button class="btn btn-success flex-grow-1 w-25 " v-on:click='changePassword()'>Save</button>
            </div>
        </div>
        <div v-else class="col-12 col-md-10">
            <h3 class="title text-center text-md-left">Edit Profile {{$myprofile['name']}}</h3>
            <form enctype="multipart/form-data" method="POSt" action="/Updateprofile">
                <div class="row" v-cloak>
                    <div class="col-md-3 text-center">
                        @csrf
                        <img class="img_profile" :src="img" style="border-radius:5px;" onclick="mainmodal_file.click()">
                        <input accept="image/*" type="file" name="main_file" ref="file" id="mainmodal_file" style="display:none"
                            v-on:change="onFileChange">
                        <br>
                        <a class="d-block mt-3" href="javascript:;" style="color:#0056b3;" onclick="mainmodal_file.click()">
                            <small>*Click to upload</small>
                        </a>

                        <a href="#" class="d-block" style="color:#0056b3;" v-on:click='isChangePassword=true'>
                            <small>*Change Password</small>
                        </a>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="col-form-label">Email :</label>
                            <div class="">
                                <input type="text" class="form-control" value="{{$myprofile['email']}}" name="email">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class=" col-form-label">Name :</label>
                            <div class="">
                                <input type="text" class="form-control" value="{{$myprofile['name']}}" name="name">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class=" col-form-label">Address :</label>
                            <div class="">
                                <textarea class="form-control" value="{{$myprofile['address']}}" name="address" s="6">{{$myprofile['member_address']}}</textarea>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class=" col-form-label">Tel :</label>
                            <div class="">
                                <input type="text" class="form-control" value="{{$myprofile['member_tel']}}" name="tel">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class=" col-form-label">Autotransfer :</label>
                            <div class="">
                                @if($myprofile['autotransfer']==0)
                                <select class="form-control" value="{{$myprofile['autotransfer']}}" name="auto">
                                        <option value="0">None</option>
                                        <option value="1">Auto</option>
                                    </select>
                                @elseif($myprofile['autotransfer']==1)
                                <select class="form-control" value="{{$myprofile['autotransfer']}}" name="auto">
                                        <option value="1">Auto</option>
                                        <option value="0">None</option>
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group mt-4 d-flex d-md-block">
                            <button type="submit" class="btn btn-success btn w-25 flex-grow-1">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
            @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong>{{ session()->get('message') }}
            </div>
            @endif

        </div>
    </div>
</div>
<script src="{{asset('/js/editprofile.js?1')}}"></script>
<script>
    var oldimg = "{{$myprofile['member_img']}}";
    var newimg = "";
    if (oldimg.length == 0) {
        newimg = "/picture/avatar.jpg";
    } else {
        newimg = "/picture/" + oldimg;
    }
    App.img = newimg;

</script>
@endsection
