@extends('layouts.layout')

@section('content')
<style>
    th,td{
        text-align: center;
    }
</style>
    <div class="content-area -admin" id="profit">
            <div class="row justify-content-center">
                <div class="col-md-10">
                        <div class="my-investment card">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h3 class="title"><i class="fal fa-chart-line"></i> รายการผลตอบแทน</h3>
                                <div v-show="allprofitLoader" class="ml-auto">
                                    <i class="fal fa-spinner-third spin-ani mr-2"></i>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-sm" v-cloak>
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">วันและเวลา</th>
                                            <th scope="col">ผลตอบแทน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(x,index) in allprofit">
                                            <td>@{{(index+1)}}</td>
                                            <th scope="row">@{{x.created_at}}</th>
                                            <td class="text-success">@{{formatMoney(x.profit)}} บาท</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
    </div>
<script src="{{asset('/js/profit.js')}}"></script>
@endsection
