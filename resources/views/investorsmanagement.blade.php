@extends('layouts.layout')

@section('content')
<style>
    .btn-group > i:hover{
        font-weight: 500;
    }
    i {
        cursor: pointer;
        margin-left:10px;
        font-size: 20px;
    }
</style>
<div class="content-area -admin" id="app">
    <div class="row">
        <div class="col-xl-12">
            <!-- My Investment -->
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-users"></i> TradeHi Investors</h3>
                    <div class="row ml-auto justify-content-end">
                        <div class="d-flex">
                            <input type="text" class="form-control" placeholder="Filter" v-model="filtext">&nbsp;&nbsp;<a href="/admin" class="btn btn-sm btn btn-secondary" style="color:white;">Back</a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table" v-cloak>
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Full Name</th>
                                <th scope="col">Class</th>
                                <th scope="col">Ratio</th>
                                <th scope="col">Fund</th>
                                <th scope="col">Return</th>
                                <th scope="col">Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for='(user,index) in filteredList'>
                                <th scope="row">@{{user.member_code}}</th>
                                <td class="text-success">@{{user.name}}</td>
                                <td class="text-success">@{{user.rate_group}}</td>
                                <td>@{{formatMoney((parseFloat(user.member_fund)/parseFloat(allfund))*100)}} %</td>
                                <td>@{{formatMoney(user.member_fund)}} Baht</td>
                                <td class="text-success">@{{formatMoney(user.member_profit)}} Baht</td>
                                <td>@{{formatMoney(parseFloat(user.member_fund)+parseFloat(user.member_profit))}} Baht</td>
                                <td>
                                    <div class="btn-group">
                                        <!--<a href="#" class="btn btn-success btn-sm"><i class="fal fa-plus"></i></a>
                                <a href="#" class="btn btn-secondary btn-sm"><i class="fal fa-exchange"></i></a>-->
                                        <a :href="'/viewinvestor/'+user.id"><i class="fal fa-eye"></i></a>
                                        <i class="fal fa-cog" v-on:click="EditRate(user.member_id,user.member_rate)"></i>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- My Investment -->
        </div>
    </div>
</div>
<div id="appmodal" v-cloak>
    <!-- use the modal component, pass in the prop -->
    <modal v-if="showModal">
        <h3 slot="header">Change group rate</h3>
        <div slot="body">
            <div class="row form-group col-xl-12">
                <select class="form-control" v-model="dataChange.rateSeleted">
                    <option v-for='item in dataGroupRow' :value="item.rate_id">@{{item.rate_group}}</option>
                </select>
            </div>
            <div class="btndeposit row form-group col-xl-12 d-flex justify-content-end">
                <button class="btn btn-default" v-on:click='showModal = false'>cancle</button>
                <button class="btn btn-success" v-on:click='UpdateMemberRate()'>save</button>
            </div>
        </div>
    </modal>
</div>

<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="$emit('close')">
            <div class="modal-container">

                <div class="modal-header">
                <slot name="header">
                    default header
                </slot>
                </div>

                <div class="modal-body">
                <slot name="body">
                    default body
                </slot>
                </div>
                <!--
                <div class="modal-footer">
                <slot name="footer">
                    default footer
                </slot>
                </div>-->
            </div>
            </div>
        </div>
        </transition>
    </script>
<script src="{{asset('/js/investors.js?2')}}"></script>
@endsection
