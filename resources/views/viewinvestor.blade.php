@extends('layouts.layout')

@section('content')
<style>
    .table-partner img {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        margin-right: 10px;
    }

    .table-sm {
        margin-bottom: 0 !important;
    }

    .padd2 {
        background-color: #ededed;

    }

    .padd2>td:nth-child(1) {
        padding-left: 4rem !important;
    }

    .padd4 {
        background-color: #e1e1e1;
    }

    .padd4>td:nth-child(1) {
        padding-left: 8rem !important;
    }

    i {
        cursor: pointer;
    }

    td {
        vertical-align: middle !important;
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        border-color: #b3b3b3 !important;
    }

    p {
        margin-bottom: 0 !important;
    }

    .img_box {
        width: 64px;
        height: 64px;
        border-radius: 50%;
    }

    .wrap-icon-mb:nth-of-type(-n+3) {
        margin-bottom: .9rem;
    }

    @media screen and (min-width: 768px) {
        .wrap-icon-mb:nth-of-type(-n+3) {
            margin-bottom: 0;
        }

        .wrap-icon-mb:nth-of-type(-n+2) {
            margin-bottom: .4rem;
        }

        .mini-title-preson strong {
            margin-right: 10px;
            display: inline-block;
        }
    }

</style>
<div class="content-area -admin" id="viewinvestor">
    <input type="text" ref="id" value="{{$id}}" hidden>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="my-fund card border-success" v-cloak>
                <div class="card-body fund-summary">
                    <div class="row">
                        <div class="col-12 col-md-6 wrap-icon-mb">
                            <div class="d-flex mt-box1"><i class="fa-2x fal fa-redo-alt"></i>
                                <div>
                                    <p class="amount text-success">
                                        @{{formatMoney(allreturn)}} บาท
                                    </p>
                                    <p class="small">
                                        ผลตอบแทน
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 wrap-icon-mb">
                            <div class="d-flex mt-box2"><i class="fa-2x fas fa-usd-circle"></i>
                                <div>
                                    <p class="amount">
                                        @{{formatMoney(fund+stag)}} บาท
                                    </p>
                                    <p class="small">
                                        ทุนเริ่มต้น
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 wrap-icon-mb">
                            <div class="d-flex mt-box3"><i class="fa-2x fal fa-usd-circle"></i>
                                <div>
                                    <p class="amount">
                                        @{{formatMoney(total)}}บาท
                                    </p>
                                    <p class="small">
                                        รวมทั้งหมด
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 wrap-icon-mb">
                            <div class="d-flex mt-box4"><i class="fa-2x fal fal fa-users"></i>
                                <div>
                                    <p class="amount">
                                        @{{formatMoney(allcom)}} บาท
                                    </p>
                                    <p class="small">
                                        คอมมิสชั่น
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="my-fund card " v-cloak>
                <div class="card-body fund-summary">
                    <div class="row ">
                        <div class="col-md-3 d-flex justify-content-center">
                            <img v-if="myData.member_img != null" class="img_box mb-4"
                                :src="'/picture/'+myData.member_img" alt="">
                            <img v-else class="img_box" src="/picture/avatar.jpg" alt="">
                        </div>
                        <div class="col-md-9">
                            <p class="mb-2 mini-title-preson"><strong>ชื่อ-สกุล :</strong>
                                <span class="d-block  d-lg-inline">@{{myData.name}}</span>

                            </p>
                            <p class="mb-2 mini-title-preson"><strong>เลขบัตรประชาชน :</strong>
                                <span class="d-block d-lg-inline ">
                                    @{{myData.passport}}
                                </span>
                            </p>
                            <p class="mb-2 mini-title-preson"><strong>เลขบัญชี :</strong>
                                <span class="d-block d-lg-inline ">
                                    @{{myData.bankname}} @{{myData.banknum}}
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-users"></i>&nbsp;Partners</h3>
                    <div class="ml-auto">
                    </div>
                </div>
                <div class="table table-responsive table-partner">
                    <table class="table table-sm" v-cloak>
                        <thead>
                            <th>#</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Profit</th>
                            <th>Commission</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding-left: 1rem!important;">
                                    <img :src="'/picture/'+myData.member_img" v-if="myData.member_img != null">
                                    <img src="/picture/avatar.jpg" v-else>

                                </td>
                                <td>
                                    <p>@{{myData.name}}</p>
                                </td>
                                <td>@{{myData.member_code}}</td>
                                <td>@{{formatMoney(myData.member_profit)}} baht</td>
                                <td>@{{formatMoney(myData.member_commission)}} baht</td>
                            </tr>
                            <tr v-for="x in row" v-show="x.show" :class="x.cl">
                                <td class="d-flex align-items-center">
                                    <i class="fa fa-plus-circle mr-1" v-on:click="ShowChild(x.id)"
                                        v-if=" x.cl=='padd2' "></i>
                                    <img :src="'/picture/'+x.img" v-if="x.img != null">
                                    <img src="/picture/avatar.jpg" v-else>

                                </td>
                                <td>
                                    <p>@{{x.name}}</p>
                                </td>
                                <td>@{{x.code}}</td>
                                <td>@{{formatMoney(x.profit)}} baht</td>
                                <td>@{{formatMoney(x.com)}} baht</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="row mb-4">
        <div class="col-12" v-cloak>
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-usd-circle"></i>&nbsp;เงินฝาก</h3>
                    <div class="ml-auto">
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-sm bg-white">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">Time</th>
                                <th scope="col">Action</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data, index) in moneyFundList">

                                <th class="text-center">@{{data.created}}</th>
                                <td>
                                    <p v-if="data.type == 0">
                                        ฝากเงิน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 1">
                                        แปลงเป็นทุน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 2">
                                        ถอนผลตอบแทน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 3">
                                        ถอนทุน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 4">
                                        ถอนคอมมิสชั่น @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else>
                                        ไม่มี type
                                    </p>
                                </td>
                                <td class="text-center">
                                    <p class="text-warning" v-if="data.status == 1">
                                        Wating
                                    </p>
                                    <p class="text-success" v-else-if="data.status == 2">
                                        Approved
                                    </p>
                                    <p class="text-danger" v-else-if="data.status == 3">
                                        Rejected
                                    </p>
                                    <p v-else>
                                        ไม่มีสถานะ
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">จำนวนเงินฝากคงเหลือ</td>
                                <td class="text-center" colspan="2">@{{formatMoney(moneyFundTotal)}} บาท</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-lg-4" v-cloak>
            <div class="my-investment card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h3 class="title"><i class="fal fa-chart-line"></i> รายการผลตอบแทน</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-sm" v-cloak>
                        <thead>
                            <tr>

                                <th scope="col">วันและเวลา</th>
                                <th scope="col">ผลตอบแทน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(x,index) in dataProfit.slice(0,10)">

                                <th scope="row">@{{x.created_at}}</th>
                                <td class="text-success">@{{formatMoney(x.profit)}} บาท</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-8" v-cloak>
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="title"><i class="fal fa-usd-circle"></i>&nbsp;ประวัติการเงิน</h3>
                    <div class="ml-auto">
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-sm bg-white">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">Time</th>
                                <th scope="col">Action</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data, index) in dataHistory.slice(0,10)">

                                <th class="text-center">@{{data.created}}</th>
                                <td>
                                    <p v-if="data.type == 0">
                                        ฝากเงิน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 1">
                                        แปลงเป็นทุน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 2">
                                        ถอนผลตอบแทน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 3">
                                        ถอนทุน @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else-if="data.type == 4">
                                        ถอนคอมมิสชั่น @{{formatMoney(data.amount)}} บาท
                                    </p>
                                    <p v-else>
                                        ไม่มี type
                                    </p>
                                </td>
                                <td class="text-center">
                                    <p class="text-warning" v-if="data.status == 1">
                                        Wating
                                    </p>
                                    <p class="text-success" v-else-if="data.status == 2">
                                        Approved
                                    </p>
                                    <p class="text-danger" v-else-if="data.status == 3">
                                        Rejected
                                    </p>
                                    <p v-else>
                                        ไม่มีสถานะ
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>










</div>
<script src="{{asset('/js/viewinvestor.js?6')}}"></script>
@endsection
