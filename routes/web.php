<?php

use Illuminate\Http\Request;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/','HomeController@index');
Route::get('/home',function(){
    return redirect()->to('/');
});
Route::get('/new_user', function(){
    return view('new-user');
});
Route::get('/dashboard', 'HomeController@index');
Route::get('register/{code}/{id}',function($code,$id){
    return view('auth.register')->with(['invite_id'=>$id]);
})->middleware('guest');
Route::post('/UpdatePassport',function(Request $request){
    $data = $request->input();
    $Userupdate = User::where('id',$data['id'])->update(['passport'=>$data['passport'],
                                            'bankname'=>$data['bank_name'],'banknum'=>$data['bank_num']]);
    return back();
});
Route::middleware(['auth'])->group(function(){
    Route::get('/EditProfile','HomeController@EditProfile');
    Route::get('/Contact','HomeController@Contact');
    Route::get('/History','HomeController@historyfund');
    Route::get('/Profit','HomeController@Profit');
    Route::post('/Updateprofile','HomeController@Updateprofile');
    Route::get('/notic','HomeController@Allnotic');
    Route::get('/Partners','HomeController@Partners');
});

Route::middleware(['auth' , 'CheckStatus'])->group(function(){
    Route::get('/investors_manage','HomeController@Investors');
    Route::get('/usermanagement','HomeController@usermanagement');
    Route::get('/setting','HomeController@Setting');
    Route::get('/admin', 'HomeController@adminindex');
    Route::get('/viewallrequest','HomeController@viewallrequest');
    Route::get('/viewinvestor/{id}','HomeController@viewinvestor');
});

