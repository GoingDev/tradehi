<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
    Route::get('/getAlluser','UserController@getAlluser');
    Route::get('/getAllmember','UserController@getAllmember');
    Route::delete('/Deluser/{id}','UserController@Deluser');
    Route::delete('/Delmember/{id}','UserController@Delmember');
    Route::post('/Edituser','UserController@Edituser');
    Route::post('/Editmember','UserController@Editmember');
    Route::get('/testnanoid','UserController@testnanoid');
    Route::get('/getmember/{id}','UserController@getmember');
    Route::get('/getProfit/{id}','UserController@getProfit');

    Route::get('/Firebase','financeController@index');
    Route::post('/AddFund','financeController@AddFund');
    Route::post('/WithDraw','financeController@WithDraw');
    Route::get('/ApproveRequests','financeController@ApproveRequests');
    Route::get('/addProfit','financeController@addProfit');
    Route::get('/ShowDeposit','financeController@ShowDeposit');

    Route::resource('setting', 'SettingController');
    Route::post('/update_member_rate', 'UserController@UpdateMemberRate');
    Route::resource('password', 'PasswordController');
});
