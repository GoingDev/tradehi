<?php

use Illuminate\Database\Seeder;
use App\ProfitRate;

class ProfitRateSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rate = new ProfitRate;
        $rate->rate_id = 1;
        $rate->rate_group = 'Standard';
        $rate->rate_return = 0;
        $rate->save();
    }
}
