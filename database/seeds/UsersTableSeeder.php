<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User;
        $user->email = 'Administrator@goingstudio.com';
        $user->name = 'Administrator';
        $user->password = Hash::make('adminadmin');
        $user->status = 1;
        $user->status = 1;
        $user->remember_token = 'vfDafK7lTjGHrE6j0C4FNHejyoHRpsaUORZ8PydbmYjNpyMDFGRI4lWu6t0G';
        $user->save();
    }
}
