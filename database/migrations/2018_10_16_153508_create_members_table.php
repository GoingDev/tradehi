<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('member_id');
            $table->string('member_code')->unique();
            $table->text('member_address')->nullable();
            $table->string('member_tel')->nullable();
            $table->string('member_img')->nullable();
            $table->integer('member_fund')->default('0');
            $table->integer('member_profit')->default('0');
            $table->integer('member_balance')->default('0');
            $table->integer('member_comission')->default('0');
            $table->integer('member_rate')->default('1');
            $table->integer('u_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
